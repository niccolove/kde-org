<ul>
<!--
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
-->

<!--
       <li>
       <strong>Debian</strong> KDE 4.3.4 packages are available in the unstable repository.
       </li>
       -->

       <li>
       <strong>Fedora</strong>
       <ul>
         <li><a href="https://admin.fedoraproject.org/updates/F12/FEDORA-2009-13285">Fedora 12</a></li>
         <li><a href="https://admin.fedoraproject.org/updates/F11/FEDORA-2009-13342">Fedora 11</a></li>
       </ul>
       </li>


	<li>
		<strong>Gentoo Linux</strong> provides KDE 4.3.4 builds in main repository.<br/>
		More details can be found on <a href="http://www.gentoo.org/proj/en/desktop/kde/kde4-guide.xml#doc_chap2">Gentoo KDE Guide</a>.
	</li>


	<!--
       <li>
                <strong>Kubuntu</strong> packages are available for 9.04 and the development Karmic release.
                More details can be found in  the <a href="http://kubuntu.org/news/kde-4.3.4">
                announcement on Kubuntu.org</a>.
        </li>
	-->

	<!--
        <li>
                <strong>Mandriva</strong> provide packages for
				<table>
				<tr>
                <td><strong>Mandriva Linux 2009.0</strong></td>
                <td><a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/Mandriva/2009.0/i586">i586</a></td>
                <td><a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/Mandriva/2009.0/x86_64">x86_64</a></td>
                <td><a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/Mandriva/2009.0/SRPMS">SRPMS</a></td>
				</tr>
				<tr>
                <td><strong>Mandriva Linux 2009.1 Spring</strong></td>
                <td><a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/Mandriva/2009.1/i586">i586</a></td> 
                <td><a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/Mandriva/2009.1/x86_64">x86_64</a></td>
                <td><a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.4/Mandriva/2009.1/SRPMS">SRPMS</a></td>
				</tr>
				</table>
                <br />
					 Please refer to <a
					 href="ftp://ftp.kde.org/pub/kde/stable/4.3.4/Mandriva/README">README</a> for more information.
				<br />
					 For Mandriva Cooker ( development ) users, 4.3.4 is available at cooker repositories.
					 Users that installed Mandriva 2010.0 alpha/beta releases should point to cooker repositories as well.
        </li>
	-->
		<li>
                <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> for
                <a href="http://download.opensuse.org/repositories/KDE:/43/openSUSE_11.2/">openSUSE 11.2</a>,
                <a href="http://download.opensuse.org/repositories/KDE:/43/openSUSE_11.1/">openSUSE 11.1</a>,
                <a href="http://download.opensuse.org/repositories/KDE:/43/openSUSE_11.0/">openSUSE 11.0</a> and
                <a href="http://download.opensuse.org/repositories/KDE:/43/openSUSE_Factory/">openSUSE Factory</a>.<br/>
                <br/>
                Please read the <a href="http://en.opensuse.org/KDE/Upgrade">Upgrade Instructions</a> how to install those.<br/>
                <br/>
                A <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live CD</a> with these packages is also available.
        </li>
<!--
        <li>
                <strong>Pardus</strong> KDE 4.2 Beta 2 packages are available for Pardus 2008 in <a href="http://paketler.pardus.org.tr/pardus-2008-test/">Pardus test repository</a>. Also
                all source PiSi packages are in <a href="http://svn.pardus.org.tr/pardus/devel/desktop/kde4/base/">Pardus SVN</a>.
        </li>
-->

<!--
        <li>
                <strong>Magic Linux</strong> KDE 4.3.4 packages are available for Magic Linux 2.5.
                See <a href="http://www.linuxfans.org/bbs/thread-182132-1-1.html">the release notes</a>
                for detailed information and
                <a href="http://apt.magiclinux.org/magic/2.5/unstable/RPMS.all/">the FTP tree</a> for
                packages.
        </li>
        -->

	<!--
        <li>
                <strong>Windows</strong> KDE 4.3.4 packages for Microsoft Windows.
                See <a href="http://windows.kde.org">windows.kde.org</a> for details. Download installer and get your kde apps on your windows desktop. Note: KDE on Windows is not in the final state, so many applications can be unsuitable for day to day use yet.
        </li>
	-->
</ul>
