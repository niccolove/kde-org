---
aliases:
- /announcements/plasma-5.8.8-5.8.9-changelog
hidden: true
plasma: true
title: Plasma 5.8.9 Complete Changelog
type: fulllog
version: 5.8.9
---

### <a name='breeze-plymouth' href='https://commits.kde.org/breeze-plymouth'>Breeze Plymouth</a>

- Fix overlapping labels on systems with different window sizes. <a href='https://commits.kde.org/breeze-plymouth/cc1019e2938a9ac9bf9a6e7149b55deec90e010a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8721'>D8721</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Make sure we remove the Transaction before is destroyed. <a href='https://commits.kde.org/discover/37e8e5db4915f55b2706e635cd14f2226d1698c8'>Commit.</a>
- Fix test run. <a href='https://commits.kde.org/discover/4fc2782a3d5de3a484a7e122b0ba3915b3685a03'>Commit.</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Also list GTK 3 themes without "gtk-3.0" subfolder. <a href='https://commits.kde.org/kde-gtk-config/1ba9f200f0fb53b1dab7e810f8a1920f18e55322'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9459'>D9459</a>
- Fix minimum height of cursor/icon theme comboboxes. <a href='https://commits.kde.org/kde-gtk-config/9615fc3994f962846791f790f9d18e2f42d774d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9457'>D9457</a>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

- Correction with the & problem in tabs. <a href='https://commits.kde.org/ksysguard/0ed4aae225c0dd67a0ae86b8e5ffdaf574ac16a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382512'>#382512</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10151'>D10151</a>
- Use OCS to retrieve Tabs from the KDE store. <a href='https://commits.kde.org/ksysguard/fdead6c30886e17ea0e590df2e7ddb79652fb328'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338669'>#338669</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8734'>D8734</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Remove unnecessary QString::arg call. <a href='https://commits.kde.org/kwin/73f5b09e3bbe62b0a09996d244eec5328f758dcc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8764'>D8764</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [kimpanel] Use automoc keyword for kimpanel-scim-panel. <a href='https://commits.kde.org/plasma-desktop/11d25c15254fe7630e46ede8c3cf8b2346bdd3bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9245'>D9245</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Notifications] Fix grouping. <a href='https://commits.kde.org/plasma-workspace/8164beac15ea34ec0d1564f0557fe3e742bdd938'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10315'>D10315</a>
- Make sure device paths are quoted. <a href='https://commits.kde.org/plasma-workspace/9db872df82c258315c6ebad800af59e81ffb9212'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389815'>#389815</a>
- Add license header to test. <a href='https://commits.kde.org/plasma-workspace/3b4a81d2fdd2df7e6e94c64a6c9f18103ec5e821'>Commit.</a>
- Sanitise notification HTML. <a href='https://commits.kde.org/plasma-workspace/5bc696b5abcdb460c1017592e80b2d7f6ed3107c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10188'>D10188</a>
- Fixed a freeze caused by certain notifications. <a href='https://commits.kde.org/plasma-workspace/5e230a6290b1ff61e54c43da48821eb2bf3192ae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381154'>#381154</a>
- Fix for xembedsniproxy crash due to NULL returned from xcb_image_get(). <a href='https://commits.kde.org/plasma-workspace/12e3568042fb365aad3eccf2fefa58bbeb065210'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9732'>D9732</a>
- Fix compilation with current cmake (git branch release). <a href='https://commits.kde.org/plasma-workspace/d02f03f46e9c380c2a18045b335a8e26f385999d'>Commit.</a>