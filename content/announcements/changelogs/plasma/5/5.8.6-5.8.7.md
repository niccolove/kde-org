---
aliases:
- /announcements/plasma-5.8.6-5.8.7-changelog
hidden: true
plasma: true
title: Plasma 5.8.7 Complete Changelog
type: fulllog
version: 5.8.7
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- Add .arcconfig. <a href='https://commits.kde.org/bluedevil/0a3d87673fa872306bc680844ffbd6c05be9659e'>Commit.</a>
- ReceiveFileJob: Don't cancel the request right after accepting it. <a href='https://commits.kde.org/bluedevil/def41978a6330097ddb191328185fa79a1d63df4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376773'>#376773</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4744'>D4744</a>

### <a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a>

- Set no stepper button for GTK3 scrollbars on gtk >=3.20. <a href='https://commits.kde.org/breeze-gtk/6ac39e82ff069f6ca4037617d906a695060f5261'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377008'>#377008</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5128'>Differential Revision: D5128</a>

### <a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a>

- Fix query for available modules. <a href='https://commits.kde.org/kde-cli-tools/19a6bb179d5fd5056f34568651bab34e9db390f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378548'>#378548</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5355'>D5355</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Implement manual focus on click. <a href='https://commits.kde.org/kscreenlocker/f8043de10b5dd94b9b931a92f3aa7167188786c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348789'>#348789</a>. Fixes bug <a href='https://bugs.kde.org/374289'>#374289</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4821'>D4821</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Fix occassional crash caused by needlessly delayed signals (bko#363224). <a href='https://commits.kde.org/kwin/55f169d1c34491f0d97c8b7a162f4ba60f6849e2'>Commit.</a> See bug <a href='https://bugs.kde.org/363224'>#363224</a>
- Fix regression for timestamp handling for Xwayland windows. <a href='https://commits.kde.org/kwin/32939fa7b5225fc5883b9888cbc6e6ddd8d6aee9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5731'>D5731</a>
- Improve the x11 timestamp handling. <a href='https://commits.kde.org/kwin/0bec9ad7337536e319c17c5684d97e1156399fdb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377901'>#377901</a>. Fixes bug <a href='https://bugs.kde.org/348569'>#348569</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5704'>D5704</a>
- Sort the themes in decoration KCM. <a href='https://commits.kde.org/kwin/f5a43877a9ea6ddad9eaa8d7498c8ea518c29c81'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5407'>D5407</a>
- Fix KWin decoration KCM showing correct index at startup. <a href='https://commits.kde.org/kwin/3709996f8a884e80139f14ed4c334b1b1c758bf1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5401'>D5401</a>
- [platforms/drm] Explicitly request event context version 2. <a href='https://commits.kde.org/kwin/4ca3d0d94370002430b5131520a11c06b23bdcaa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5380'>D5380</a>
- Fix crash on dragging titlebar buttons in System Settings. <a href='https://commits.kde.org/kwin/1bfe1164f41dc328d54f7dc6ed13b2fabfde09f6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374153'>#374153</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5117'>D5117</a>
- [kcm_kwindecoration] Respect theme colors in buttons. <a href='https://commits.kde.org/kwin/c3362fe866dd6368855905b8fbc6e828197cb538'>Commit.</a>
- Move the view at the correct index at startup. <a href='https://commits.kde.org/kwin/892f398b10abd5bc1c3105e4b917cc2c7b396990'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4703'>D4703</a>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

- Don't reset the view to the top on gaining focus. <a href='https://commits.kde.org/libksysguard/acb088749af1678814fe7718886ca36209b02e79'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363420'>#363420</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Make sure the "default" sheme is actually default. <a href='https://commits.kde.org/plasma-desktop/fcecf928409c7b9b00f1812b308c1bbba8e93bdc'>Commit.</a>
- Possible to edit the default color scheme. <a href='https://commits.kde.org/plasma-desktop/c8d7f0363a568b6a4c5f0d7061a5858028a0526d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5406'>D5406</a>
- Fix -Wreorder warning. <a href='https://commits.kde.org/plasma-desktop/f7cbf298f9bf430533e184ac1ba218154ecced08'>Commit.</a>
- Backport 5.9/Master's GroupDialog code to 5.8. <a href='https://commits.kde.org/plasma-desktop/76034a4cd3b2ecd2d97802037ea6db0756651786'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378042'>#378042</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5287'>D5287</a>
- [Task Manager] Keep entry highlighted when context menu or group dialog is open. <a href='https://commits.kde.org/plasma-desktop/72f5d68657af460cf7e50c7c9ba231c1bbd3d694'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5260'>D5260</a>
- [Folder View] Use toDisplayString which strips passwords. <a href='https://commits.kde.org/plasma-desktop/b9eedbe78c7967b321a4104c93162c89dbbbd647'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5226'>D5226</a>
- [Applet Alternatives] Don't animate highlight resize. <a href='https://commits.kde.org/plasma-desktop/02137a269d7746c47abf4f53c904d7bced6ad017'>Commit.</a>
- [Applet Alternatives] Fix icon size. <a href='https://commits.kde.org/plasma-desktop/e28ea7a47c9669f232b05451e20d001ca61cd2c4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5213'>D5213</a>
- Add missing member initialization. <a href='https://commits.kde.org/plasma-desktop/0754179a134bb9be149cd7a932188722c0b8f66a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378016'>#378016</a>
- [Kicker] Fix highlighting favorites. <a href='https://commits.kde.org/plasma-desktop/db297ab5acb93f88c238778e8682effe3032bf4f'>Commit.</a> See bug <a href='https://bugs.kde.org/377652'>#377652</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5064'>D5064</a>
- [Folder View] Explicitly set prefix as empty in normal state. <a href='https://commits.kde.org/plasma-desktop/b7ce1a4bc1d050ee03837bac77d1a31c3b84c175'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377441'>#377441</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4998'>D4998</a>
- Do apply margin if in right-to-left mode. <a href='https://commits.kde.org/plasma-desktop/d37b57dae09d1d9d0e2b26101514ad216139c063'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376529'>#376529</a>
- [AppletAppearance] Silence warning. <a href='https://commits.kde.org/plasma-desktop/02ccf21432f58836df72105a39aa0db2287b62cc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4854'>D4854</a>
- [Folder View] Fix action button hover and pressed state. <a href='https://commits.kde.org/plasma-desktop/5b9985501cb67107bd329a04b6558397ba0b78ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4915'>D4915</a>
- Disable editing of 'Global Shortcuts' component names. <a href='https://commits.kde.org/plasma-desktop/cafc7d5a448a19047762af3eb0d5e5a495ef58ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376935'>#376935</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4905'>D4905</a>
- Fix switching categories via the filter listview on touchscreens. <a href='https://commits.kde.org/plasma-desktop/d85dd61627bfa2e2e906fddb16d7a9bee71375f8'>Commit.</a>
- Fix default fixed font in fonts kcm. <a href='https://commits.kde.org/plasma-desktop/d02de0db36a35cc2e66ff91b8f5796962fa4e04e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4794'>D4794</a>
- Set root.dragging to false during reset. <a href='https://commits.kde.org/plasma-desktop/2695842c02bb2ca8e3af9bcdc0916dc1d6d108cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376633'>#376633</a>
- Revert "use KPluginMetaData::readStringList". <a href='https://commits.kde.org/plasma-desktop/a96f503bcbb711d1043bcee9e81645b6d4e02dac'>Commit.</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Context: Use DBus service watcher to reconnect to pulseaudio. <a href='https://commits.kde.org/plasma-pa/60dc3a5cce37ed72a48693032097df3cff90def9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358772'>#358772</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5171'>D5171</a>
- KCM: Fix 100% volume label position with RTL. <a href='https://commits.kde.org/plasma-pa/86c4c2921104cd8a644e5ef92217f994b239840a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5548'>D5548</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Remove useless debug. <a href='https://commits.kde.org/plasma-workspace/c868f173a9c61b1ebc3137d0b8a3ac9126405c39'>Commit.</a>
- Don't check for panel visible during startup. <a href='https://commits.kde.org/plasma-workspace/d602198f6807bbfa175b6aaaa1bdf1708fce703e'>Commit.</a> See bug <a href='https://bugs.kde.org/377280'>#377280</a>
- [Weather] Fix term used for thunderstorm in bbcukmet data db. <a href='https://commits.kde.org/plasma-workspace/12a82fcba5672ee6b4473dfc6d0a84280a2bfbbb'>Commit.</a>
- [Weather] Add missing strings from bbcukmet data db. <a href='https://commits.kde.org/plasma-workspace/1784cc88e547a6cdf5001cfa4fef704eac34e7c6'>Commit.</a>
- [Weather] Remove duplicated/bad strings from bbcukmet data db. <a href='https://commits.kde.org/plasma-workspace/1c9c6c8aa37e957cac82f08ccd6220dd41a5f2d3'>Commit.</a>
- [Weather] Fix lookup of bbcukmet translations strings, stored in lowercase. <a href='https://commits.kde.org/plasma-workspace/c9d571d2d5b28bbd7a9a2602deb929585095d1cf'>Commit.</a>
- Update unit test to match change to reselecting top entry. <a href='https://commits.kde.org/plasma-workspace/13f63a3f3897abfbec64066e7a3f3af22661329f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5570'>D5570</a>
- Revert "launch autostart apps in ksmserver using KRun". <a href='https://commits.kde.org/plasma-workspace/ea3f87c5df0251838da71c473fd7b790c932d8b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379254'>#379254</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5618'>D5618</a>
- Selecting the topmost klipper item should always set it as clipboard contents. <a href='https://commits.kde.org/plasma-workspace/f78b0869f01d255d0e17a0a246e5a9456f2bba4e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348390'>#348390</a>. Fixes bug <a href='https://bugs.kde.org/251222'>#251222</a>
- Use KProcess instead of QProcess to launch apps in ksmserver. <a href='https://commits.kde.org/plasma-workspace/17eeb58ae42596195d6a841c06d17721b7af2c14'>Commit.</a> See bug <a href='https://bugs.kde.org/369391'>#369391</a>. See bug <a href='https://bugs.kde.org/370528'>#370528</a>
- Launch autostart apps in ksmserver using KRun, not QProcess. <a href='https://commits.kde.org/plasma-workspace/0f19e92f3e85d064de9cebf280fa8e085485c2e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369391'>#369391</a>. Fixes bug <a href='https://bugs.kde.org/370528'>#370528</a>
- Recognize the WM even if given with a full path. <a href='https://commits.kde.org/plasma-workspace/7fca524e0846fd16558a4ac085afa551a9f22c99'>Commit.</a> See bug <a href='https://bugs.kde.org/377756'>#377756</a>
- [Media Controller] Enforce PlainText format. <a href='https://commits.kde.org/plasma-workspace/4d3760c8aa9d6a99e5239019fb9a29aab2e22896'>Commit.</a>
- Fix off-by-one. <a href='https://commits.kde.org/plasma-workspace/e34b0064d6ff787270127d261be65c3b6780b31f'>Commit.</a> See bug <a href='https://bugs.kde.org/373075'>#373075</a>
- Media Controller can now properly handle and seek long tracks (&gt; 30 minutes). <a href='https://commits.kde.org/plasma-workspace/550860f6366cc99d3f0ff19f74fd3fc3d1bfc0ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377623'>#377623</a>
- When deleting the panel the notifications applet is in, the containment is being destroyed but the corona. <a href='https://commits.kde.org/plasma-workspace/58206408a35a0900b5678231b7aaf490b2a3ec10'>Commit.</a> See bug <a href='https://bugs.kde.org/378508'>#378508</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5321'>D5321</a>
- Correctly handle when a new primary screen displaces the old. <a href='https://commits.kde.org/plasma-workspace/ef12ad389ee48208e7a7c8520dc3a05864d2fe87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5323'>D5323</a>
- Fix TasksModel.anyTaskDemandsAttention not updating on window closure. <a href='https://commits.kde.org/plasma-workspace/628593db4c65560ace95759ffca5ce0920de621e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378254'>#378254</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5261'>D5261</a>
- [Calculator Runner] Use "approximate" approximation mode. <a href='https://commits.kde.org/plasma-workspace/34b74d7b5dac9dd458a960e392954ac6c30df3d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/277011'>#277011</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4290'>D4290</a>
- [OSD] Allow disabling OSD through config file. <a href='https://commits.kde.org/plasma-workspace/37ac27c59d97db3bb173ae1786a639cce0c6feb6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D1770'>D1770</a>
- Manage mouse events when out of the window. <a href='https://commits.kde.org/plasma-workspace/7f2dceb95c5812207c87f4b61d5a90e0306603d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377545'>#377545</a>
- Don't emit availableScreenRectChanged when quitting. <a href='https://commits.kde.org/plasma-workspace/721fcd3d309171fde2dc37b84939f7657db91de3'>Commit.</a>
- Try proper order of corona()->availableScreenRect(). <a href='https://commits.kde.org/plasma-workspace/bcb6b86d3ab8bd196877473af343b72c53e4f932'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377298'>#377298</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4991'>D4991</a>
- Ignore NoDisplay=true .desktop entries when matching by Name. <a href='https://commits.kde.org/plasma-workspace/8028a500e3b21ec96751b73e402df2f5be459e9e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4928'>D4928</a>
- Try harder to make the selected wallpaper visible. <a href='https://commits.kde.org/plasma-workspace/ff602030827dcada48f10693970c020e14369e11'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4648'>D4648</a>
- [Logout Screen] Show suspend button only if supported. <a href='https://commits.kde.org/plasma-workspace/8bc32846a5a41fa67c106045c43bb8c4af7e7e6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376601'>#376601</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Set the correct desktop file name when in a KDE session. <a href='https://commits.kde.org/systemsettings/f61f9d8c100fe94471b1a8f23ac905e9311b7436'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5006'>D5006</a>

### <a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a>

- Make sure the new avatar is always saved. <a href='https://commits.kde.org/user-manager/826e41429917f6c1534e84e8b7821b8b53675910'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350836'>#350836</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5779'>D5779</a>