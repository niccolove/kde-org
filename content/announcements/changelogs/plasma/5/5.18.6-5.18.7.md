---
title: Plasma 5.18.7 complete changelog
version: 5.18.7
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Only turn http[s] links into clickable links. [Commit.](http://commits.kde.org/discover/fcd3b30552bf03a384b1a16f9bb8db029c111356) 
+ Fix build with Qt 5.12. [Commit.](http://commits.kde.org/discover/d4e207a620318d60dbfb75ba4e927cc02480a320) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Fix outdated API key for flickr provider. [Commit.](http://commits.kde.org/kdeplasma-addons/90a993061f0987bb46d42f2c5e9bf9c0d51291b6) See bug [#427566](https://bugs.kde.org/427566)
{{< /details >}}

{{< details title="kwallet-pam" href="https://commits.kde.org/kwallet-pam" >}}
+ Avoid socket listening error. [Commit.](http://commits.kde.org/kwallet-pam/82a93cca1da4d2a4eeb450804c4895f43053d1f1) Fixes bug [#400929](https://bugs.kde.org/400929)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Detect softpipe and llvmpipe on Mesa 20.2+. [Commit.](http://commits.kde.org/kwin/0e9fe61c325a31653845f035986543b0f8744758) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Pass Qt::MatchExactly when calling QAbstractItemModel::match for strings. [Commit.](http://commits.kde.org/plasma-desktop/edf0b4ae99c5957667d58dc55fdeac69087331d3) 
+ [kcms/activities] Use correct method to set active global shortcut. [Commit.](http://commits.kde.org/plasma-desktop/52c5290bf48bb96b9e0073880c72abf7c9c14c50) Fixes bug [#374575](https://bugs.kde.org/374575)
+ [folder view] Fix display on not-skinny vertical panels. [Commit.](http://commits.kde.org/plasma-desktop/dec75f0da52562b719b4d259b9061a3665b6c16b) Fixes bug [#406806](https://bugs.kde.org/406806)
+ [folder view] de-duplicate switch width/height logic. [Commit.](http://commits.kde.org/plasma-desktop/893aa4d83eaa6b8d959ed89a70718bddf97f9390) 
+ Reparse the key repeat rate config when we try to load it. [Commit.](http://commits.kde.org/plasma-desktop/e29635deb6ab3774fbb7d5e9dd72ae479f4aa985) Fixes bug [#418175](https://bugs.kde.org/418175)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [libtaskmanager] Fix incorrect URL comparison when writing back a new launcher order. [Commit.](http://commits.kde.org/plasma-workspace/2ea791b2fb6c125ea25258cbbcf4ae60daef460a) Fixes bug [#426880](https://bugs.kde.org/426880)
+ Startplasma-wayland: Fix XKB variable setup. [Commit.](http://commits.kde.org/plasma-workspace/b75f2119cd741cc1fc2cd64351d7b0b40a30fc45) 
+ [lookandfeel] Fix wake existing screensaver mode with key presses. [Commit.](http://commits.kde.org/plasma-workspace/13057013d55ae19e76d29b9edc96510e52da2a7a) Fixes bug [#431286](https://bugs.kde.org/431286)
+ [lookandfeel] Avoid rendering invisible contents. [Commit.](http://commits.kde.org/plasma-workspace/697b103f5fad5b40b207eabcbce162d6672f5d91) Fixes bug [#347772](https://bugs.kde.org/347772)
+ Fix Environment Canada weather domain. [Commit.](http://commits.kde.org/plasma-workspace/e27ba2d13c4d1f50eb3d83271a1654401d9e0035) 
{{< /details >}}
