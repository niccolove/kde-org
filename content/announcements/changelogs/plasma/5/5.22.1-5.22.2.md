---
title: Plasma 5.22.2 complete changelog
version: 5.22.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: properly notify about updates. [Commit.](http://commits.kde.org/discover/2a3c864386758e6bc8e973002323020f037ec2fb) Fixes bug [#438670](https://bugs.kde.org/438670)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Wayland: Implement activities window rule. [Commit.](http://commits.kde.org/kwin/6be5055bdf99f85c1c6eee180451536ee64c79ba) Fixes bug [#439022](https://bugs.kde.org/439022)
+ Scenes/opengl: Update unstyled EffectFrame texture only when rendering. [Commit.](http://commits.kde.org/kwin/8e29304235407a641f61805ccf4493872c835653) 
+ Platforms/drm: some improvements for logging. [Commit.](http://commits.kde.org/kwin/7a48ec6e66f080e8d53fbd63ffcfecd40ecf628c) 
+ XdgSurfaceClient: set to current activity and desktop in constructors. [Commit.](http://commits.kde.org/kwin/a2962635bec6c6416e2dc754ec7d727a6b6db7bb) Fixes bug [#435745](https://bugs.kde.org/435745)
+ Platforms/drm: fix mistake in DrmGbmBuffer. [Commit.](http://commits.kde.org/kwin/5a9959c7a0706046c818e38465db6f6412438c0c) 
+ Platforms/drm: show cursor when exiting dpms. [Commit.](http://commits.kde.org/kwin/9a210ec018ebe32090595e7c28b088b55e11259d) Fixes bug [#438824](https://bugs.kde.org/438824)
+ [kcmkwin/kwindesktop] Drop local back for Formlayout label width. [Commit.](http://commits.kde.org/kwin/c6d2f4ced230aece621715f046f0a501b5e9884f) Fixes bug [#438504](https://bugs.kde.org/438504)
+ Guard gbm and eglstreams parts for non-gbm or non-eglstreams systems. [Commit.](http://commits.kde.org/kwin/df1eaeb24dae909d4c0e234709e69b354c3646e0) 
+ Effects: Fix interpolated values for redirected animations. [Commit.](http://commits.kde.org/kwin/9169445b9bbb91f27577baf7ecb247c2e31d5b0a) Fixes bug [#438368](https://bugs.kde.org/438368)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Fix error about "reversed" not being defined. [Commit.](http://commits.kde.org/milou/186fe7faee21a0268ead6840dc574bcbca8d1f9b) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [kcms/landingpage] Give up on vertical centering and let it scroll. [Commit.](http://commits.kde.org/plasma-desktop/3465955554beb28b05644fbfe6e891cb3fe17442) Fixes bug [#438171](https://bugs.kde.org/438171)
+ [Task Manager] Show window title in tooltip when player title is different. [Commit.](http://commits.kde.org/plasma-desktop/2e499ee4a244777648c1ae621499c8f39ec23cd3) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix copy-paste error. [Commit.](http://commits.kde.org/plasma-workspace/350708a33c6e37b4d55fe01a264287e95c6981a6) 
+ [Notifications] Strip trailing slash for fallback URL path. [Commit.](http://commits.kde.org/plasma-workspace/09ae7c389ef0a57e45e83e777dd5457731a73327) 
+ [Notifications] Restore "details" reference. [Commit.](http://commits.kde.org/plasma-workspace/9fcfc547a5c1147b9767cd0492f372c0951d7e79) 
+ [Notifications] Avoid showing "0 seconds remaining". [Commit.](http://commits.kde.org/plasma-workspace/836c1391c7168e3bacc715d3c8675d244bdb2b82) 
+ Init ShellCorona after KDBusService. [Commit.](http://commits.kde.org/plasma-workspace/2a67a010677323a0ec2569510207dbd34031b816) Fixes bug [#408310](https://bugs.kde.org/408310)
{{< /details >}}

