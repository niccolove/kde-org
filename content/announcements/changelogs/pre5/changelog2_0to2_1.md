---
aliases:
- ../changelog2_0to2_1
hidden: true
title: KDE 2.0 to 2.1 Changelog
---

This page tries to present as much as possible of the problem
corrections and feature additions that occurred in KDE between the 2.0
and 2.1 releases.

<h3>abbrowser</h3><ul>
  <li>Uses a <i>dcop</i> interface</li>
  <li>Now a KUniqueApplication - only one instance can be run at any time</li>
</ul>
<h3>aRts</h3><ul>
  <li>Various performance improvements, most important: optimizations for
      x86 processors, tuning of the mp3 decoding
  <li>Improved artscontrol: gui improvements, status display, reverb
      effect, controlling midi support (including synthesis instruments)</li>
  <li>Lots of work on new aRts modules line effects and filters - mostly
      everything that was available in aRts-0.3.4.1 is ported</li>
  <li>Usability improvements on artsbuilder, quite some examples added</li>
  <li>New code for handling midi data (artsmidi.idl) and gui (artsgui.idl)</li>
  <li>Moved .mcopclass files from $(libdir) to $(libdir)/mcop</li>
  <li>Fix for PAS16 cards</li>
  <li>Rewritten configuration module, more intuitive, more configurable,
      applying settings is possible without relogin now</li>
  <li>Support for 8bit soundcards</li>
  <li>Made it easy to adapt to new methods of doing audio i/o</li>
  <li>support for audio output via ALSA</li>
</ul>
<h3>Control Center</h3><ul>
  <li>Extended kcmshell (supports multiple modules, using an icon list on the left)</li>
  <li>Theme Manager</li>
  <li>List of installed IO-slaves</li>
</ul>
<h3>Icons</h3><ul>
  <li>The standard icons are now 34x34 instead of 32x32 to allow the use of the
    alpha channel for shadows.</li>
  <li>Support for semi-transparency (alpha-blending) on small images and icons</li>
  <li>Small Hicolor-icons / Better icons</li>
</ul>
<h3>KDE Address Book (kab)</h3><ul>
  <li>Now provides searching the database using wildcards or regular expressions to easier find people</li>
  <li>Now provides printing of single entries, all entries or the entries that matched a search</li>
  <li>Now provides exporting the database to HTML files</li>
  <li>Now features multiple toolbars to be more customizable</li>
  <li>Displays much more icons in its menubar than before</li>
</ul>
<h3>kdeadmin</h3><ul>
  <li>lilo-config added</li>
  <li>ksysctrl is now a kcontrol module and have a improved pci detection</li>
</ul>
<h3>kdegames</h3><ul>
  <li>kjezz added</li>
  <li>kwin4 added</li>
  <li>Configurable card decks added</li>
</ul>
<h3>kdegraphics</h3><ul>
  <li>Pixie added</li>
</ul>
<h3>kdelibs</h3><ul>
  <li>New class: KCommand (undo/redo support)</li>
  <li>New class: KToolbarPopupAction (action with popup in toolbar and single item in menu)</li>
  <li>New class: KEditListBox (editable QListBox with Up, Down, Add, Remove buttons)</li>
  <li>KListView: support for moving items in a tree</li>
</ul>
<h3>Keystone</h3><ul>
  <li>Now has a toolbar</li>
  <li>Support for cut and paste between local and remote sessions</li>
  <li>Full screen mode icons now reflect the current state</li>
</ul>
<h3>Kicker</h3><ul>
  <li>Support for icon zooming. When this feature is enabled the icons in the
    panel zoom to a larger size when the mouse passes over them.</li>
  <li>New panel extension API</li>
  <li>External taskbar re-added</li>
  <li>Support for sub-panels. These sub-panels can contain buttons, applets etc.
    as usual, they can also be a different size to the main panel if you want.</li>
  <li>Support for WindowMaker dock applets</li>
  <li>Kasbar included. Now uses the extension API and supports transparency, window
    menu, and window thumbnails.</li>
  <li>Rewritten and simplified configuration dialog</li>
  <li>Improved mini pager applet</li>
  <li>Improved taskbar applet</li>
  <li>Improved drag and drop</li>
  <li>Recent applications menu</li>
  <li>Improved menu button handling (servicemenus)</li>
</ul>
<h3>KIO</h3><ul>
  <li>Support for proxies that need authentication</li>
  <li>IO-slave for audio CD's with CDDB support</li>
  <li>Floppy IO-slave, enter floppy:/ to access your floppy drive</li>
  <li>New SMB IO-slave, which replaces the old one, it is now a wrapper around
  smbclient, so you need to have samba installed, it is faster, more compatible, but read-only</li>
  <li>Much faster copying, moving and deleting of many small files</li>
  <li>Man page IO-slave: faster, nicer, better</li>
  <li>Bugfixes to various IO-slaves</li>
</ul>
<h3>klipper</h3><ul>
  <li>It is now possible to replay actions (if available and enabled) when selecting a history item</li>
  <li>Mark up the history item currently in clipboard</li>
</ul>
<h3>KNode</h3><ul>
  <li>full support for non iso-8859-x charsets</li>
  <li>basic PGP/GnuPG support</li>
  <li>full <a href="http://www.xs4all.nl/~js/gnksa/">GNKSA</a> compliance</li>
  <li>followup and reply at same time is now possible</li>
  <li>the moderation status of newsgroups is detected and displayed</li>
  <li>rot13 de-/encoding</li>
  <li>server specific identities</li>
  <li>selection of multiple articles/groups in the listview</li>
  <li>improved keyboard handling in the configuration dialog</li>
  <li>selection dialogs for sort column (F7) and filter (F6) that can be reached with a keyboard shortcut</li>
  <li>the article line count can now be displayed in the listview</li>
  <li>improved color and font configuration</li>
  <li>the interpretation of text format tags can now be disabled</li>
  <li>support for other webbrowsers (Mozilla, Opera, user-defined)</li>
  <li>added a context menu for the article pane</li>
  <li>
  <li>improvements for the composer:
    <ul>
      <li>switching from news article to mail and back on the fly</li>
      <li>paste as quotation</li>
      <li>add/remove quotation</li>
      <li>box quote support</li>
      <li>status bar</li>
      <li>word wrap can now be disabled</li>
      <li>it's now possible to get the original text if the automatic rewrap has gone wrong</li>
      <li>the user can determine which part of the original message is quoted by selecting this part before hitting reply</li>
      <li>implemented a lot of sanity checks<li>
    </ul>
  </li>
  <li>changed the way the draft folder works, articles can now be saved as draft without any sanity checks, but they have to be edited in the composer again before the are sent.</li>
  <li>'>' instead of '> ' as quote prefix for quote levels >= 2</li>
  <li>numerous bugfixes</li>
  <li>code cleanups</li>
</ul>
<h3>KMail</h3><ul>
  <li>Drafts folder</li>
  <li>Mailinglist aware folders</li>
  <li>Recognition of other reply and forward prefixes besides Re: and Fwd:</li>
  <li>Correct reply phrases possible in a different language than the GUI</li>
  <li>"Set Reply-To" filter rule</li>
  <li>Execute filter rule now can pass message parts to the application</li>
  <li>Full support for mails in all languages and charsets supported by Qt</li>
  <li>Problem with NFS mounted home directory solved</li>
  <li>Support for mails with a binary body (attachments without text)</li>
  <li>Cipher text display after writing a pgp message</li>
  <li>GUI configuration of local accounting locking preferences</li>
  <li>GUI configuration of preferred PGP tool</li>
  <li>Lots of other bugfixes</li>
</ul>
<h3>Konqueror</h3><ul>
  <li>Improved file name completion</li>
  <li>In-place renaming of files</li>
  <li>Image thumbnail feature extended to support text and HTML files as well.</li>
  <li>Bookmarks are now stored as XML, following the XBEL standard (old-style bookmarks are automatically imported)</li>
  <li>New bookmark editor</li>
  <li>Improved support for window profiles: these now have dynamically assigned
    keyboard shortcuts, and view linking is now correctly saved in the profile.</li>
  <li>Toolbar settings are now saved automatically</li>
  <li>Devices can now appear in the directory tree and will be mounted on demand</li>
  <li>Automatically chooses web browsing profile when opened on HTML files (even
    if they're local)</li>
  <li>The properties dialog now supports multiple files, and even allows to apply
    permission changes to subdirectories and their content</li>
  <li>Real speed improvement in the directory views</li>
  <li>Persistent history, part of the left-pane tree</li>
  <li>Drag and drop improvements (drop onto a web page, drag the location label...)</li>
  <li>Embedded "find" dialog, showing the result in any view mode (icon, list etc.)</li>
  <li>Improved keyboard navigation and selection for the listviews</li>
  <li>Support for "favorite icon": Konqueror can look for an icon on the webserver to include
    it with bookmark entries and show it in the locationbar for the appropriate site</li>
  <li>New shell command plugin which allows you to run a command then captures the output
    in a window. This is an easy way to use shell commands like wc, grep etc. from within
    Konqueror.</li>
  <li>Improved copying/moving of files using the F7/F8 keys, much like in MC</li>
</ul>
<h3>KHTML</h3><ul>
  <li> Java support improved - better security, now uses the Java 1.2 URLClassLoader
    and friends to download applet resources. Note that the code now requires at least
    JDK 1.2 (or compatible).</li>
  <li> The Java support now is compatible with the JSSE classes from Sun that allow Java
    Applets to work over https.  In order to enable this, get the three jar files: jcert.jar,
    jnet.jar, and jsse.jar from the zip file you can download from http://java.sun.com/products/jsse/.
    Simply copy them to the $KDEDIRS/share/apps/kjava/ directory and they will be automatically
    detected and used.&nbsp;If the jvm is already configured to use them, it will work as well.</li>
  <li>Support for XML in combination with CSS.
  <li>New 'transitional mode' used when parsing old HTML, allows greater compatibility
    with badly-formed pages etc. without causing a performance hit for pages that
    use HTML correctly. This change is invisible to users.</li>
  <li>JavaScript support for cookies, plugins and mimetypes. Labels, function
    expressions and additional Math and Array properties.</li>
  <li>Better configuration options for charsets and font sizes.
  <li>Lots of overall improvements in the HTML, CSS and JavaScript support.
  <li>Many other fixes</li>
</ul>
<h3>kpackage</h3><ul>
  <li>rpm 4.0.1 support added</li>
</ul>
<h3>kpat</h3><ul>
  <li>Uses the new card decks</li>
</ul>
<h3>kppp</h3><ul>
  <li>Fixed sorting in log viewer. Each column is sortable now</li>
  <li>Solaris port. Patches contributed by Keith Refson and Stephen Usher</li>
</ul>
<h3>KDE System Guard (KSysGuard)</h3><ul>
  <li>DancingBar widget</li>
  <li>APM sensors</li>
  <li>lm sensors support</li>
  <li>some minor usability enhacements</li>
</ul>
<h3>KView</h3><ul>
  <li>Port to KActions - it now has a toolbar</li>
  <li>Fixed Bugs: Saving a file works now, Drag and Drop works with multiple files</li>
  <li>Shuffle function in the image list ported</li>
  <li>Sorting function for the image list added</li>
  <li>Actions that shouldn't be available are now disabled (like saving if there's no
  image opened)</li>
  <li>Resize of image or window on opening a file now works a whole lot better - fixed
  the bug, that kview would move offscreen when showing a slideshow</li>
  <li>KView now handles transparent images better</li>
  <li>Save/Load of image lists added</li>
  <li>Images from a http/ftp address are only loaded once</li>
  <li>Drag and Drop can now handle images</li>
  <li>Opening multiple files in one file dialog</li>
</ul>
<h3>KWIN</h3><ul>
  <li>Netwm implementation updated to the finalized specification</li>
  <li>Reworked default window decoration style</li>
  <li>More sophisticated LMB/MMB/RMB maximization</li>
  <li>It is now possible to (de)activate the Ctrl+TAB hotkey (for rolling through all desktops)</li>
  <li>Shade/unshade at mouse hovering over titlebar</li>
  <li>Graphical configuration  was added for:
      <ul>
	<li>behaviour of the Alt+TAB functionality</li>
	<li>(de)activation of the Ctrl+TAB hotkey</li>
	<li>moving/resizing maximized windows allowance</li>
	<li>animated and hovering shading</li>
      </ul>
    </li>
    <li>Support for per window storage of current status: size, position, maximization, stickiness etc.</li>
  <li>New Win2K-like window decoration style</li>
</ul>

<h3>LAN Browsing (part of kdenetwork)</h3><ul>
<li>The LAN Browsing software provides something similar to the Network
  Neighbourhood under Windows, but relying only on the TCP/IP protocol stack.</li>
<li>LISa is a daemon which you have to run locally on your system, if you
configured it correctly, it will provide the information about your network neighbourhood.</li>
<li>the IO-slave lan:/ makes the information from LISa available to all KDE programs.</li>
<li>ResLISa and rlan:/ are versions of LISa and lan:/ for environments with stricter
security rules.</li>
<li>Please read the documentation carefully, if you still have problems, contact <a href=mailto:&#00110;eun&#x64;&#x6f;r&#x66;&#64;kde.o&#114;g>Alexander Neundorf</a></li>
</ul>

<h3>noatun</h3><ul>
  <li>Noatun, the next-generation KDE media player is now included in KDE, and
      will replace kaiman
</ul>
<h3>SysV-Init Editor (KSysV)</h3><ul>
  <li>Support for KDE-standard "Open With..." feature for services (scripts)</li>
  <li>Minor drag &amp; drop bugfixes (deleting entries via the trashcan didn't result
  in a "changed" state, so the new configuration couldn't be saved)</li>
  <li>New configuration dialog (created using Qt Designer)</li>
  <li>Configuration wizard (executed on first startup)</li>
  <li>Fixed show/hide behavior of the "Tools" dialogs ("Start Service", "Stop Service", ...)</li>
  <li>Logfile printing works again</li>
  <li>"Properties..." are accessible via the menu/keyboard</li>
</ul>

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>