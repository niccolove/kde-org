2007-01-17 20:39 +0000 [r624715]  pino

	* branches/KDE/3.5/kdeedu/kig/configure.in.in: Python 2.5 support,
	  thanks Jonathan Riddel for the patch!

2007-01-21 15:10 +0000 [r625898]  pino

	* branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquiz.desktop,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/Makefile.am: Add the proper
	  basic categories, and install in the right place. Peter, could
	  you please add the right category for it (eg Languages), or add
	  nothing to be in Misc? CCMAIL: peter.hedlund@kdemail.net

2007-01-22 16:29 +0000 [r626260]  annma

	* branches/KDE/3.5/kdeedu/kverbos/kverbos/data/de/verbos.verbos,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/data/en/verbos.verbos:
	  wow, a chance I got my noise in KVerbos as the data was not UTF8
	  thus could not be loaded!

2007-01-25 15:27 +0000 [r627053]  pino

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.dtd: fix the dtd

2007-02-09 20:06 +0000 [r632028]  annma

	* branches/KDE/3.5/kdeedu/klettres/klettres/klettres.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/data/langs/nds.txt
	  (added),
	  branches/KDE/3.5/kdeedu/klettres/klettres/data/langs/Makefile.am:
	  add Low Saxon support, thanks to Soenke Dibbern for recording the
	  sounds

2007-02-11 21:29 +0000 [r632668]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/src/boxasker.cpp,
	  branches/KDE/3.5/kdeedu/kgeography/src/boxasker.h: small
	  ?usability¿ improvement, make return == pressing the accept
	  button on boxaskers

2007-02-13 15:24 +0000 [r633229]  pino

	* branches/KDE/3.5/kdeedu/libkdeedu/kdeeduui/kdeeduglossary.cpp:
	  Visually select the new item when switching to another item
	  through the References links. BUG: 112414

2007-02-21 14:30 +0000 [r635965-635964]  annma

	* branches/KDE/3.5/kdeedu/klettres/klettres/klettres.cpp: fix
	  loaded language, previously reloaded only English

	* branches/KDE/3.5/kdeedu/blinken/src/button.cpp: setting a group
	  makes the config actually store the change of key and use it.
	  Previously it was written in config file but not read, it came
	  always back to defaults

2007-02-21 23:18 +0000 [r636116]  pino

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/x-kvtml.desktop: no
	  need to specify a mimetype twice, especially if the second one is
	  wrong

2007-03-01 18:18 +0000 [r638362]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/asia.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/data/world.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/src/mapsdatatranslation.cpp:
	  Rangoon is not the capital of Myanmar anymore BUGS: 142329

2007-03-01 18:45 +0000 [r638373]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouch.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouch.h: Fix: Bug 118862:
	  'accents dead keys do not work'

2007-03-12 21:55 +0000 [r641939]  mueller

	* branches/KDE/3.5/kdeedu/kanagram/src/kanagramgame.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/kbruch.cpp,
	  branches/KDE/3.5/kdeedu/kturtle/src/executer.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/entry-dialogs/FromToEntryPage.cpp,
	  branches/KDE/3.5/kdeedu/kbruch/src/task.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/indistd.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/spotlight2kvtml.cpp:
	  the usual "daily unbreak compilation"

2007-03-21 17:29 +0000 [r645106-645104]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/TZrules.dat: Backport
	  new DST rules for the US

	* branches/KDE/3.5/kdeedu/kstars/kstars/kssun.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/ksplanetbase.cpp: Fixing
	  bug #139676. There was a bug in the calculation of a planet's
	  phase angle, which resulted in nonsense magnitudes (most
	  obviously in Mars). For the Sun, the apparent magnitude was never
	  set, so it displayed 0.0 instead of the true value of -26.73. I
	  will forward-port to trunk as well. BUG: 139676 CCMAIL:
	  kstars-devel@kde.org

2007-03-22 15:55 +0000 [r645428]  johach

	* branches/KDE/3.5/kdeedu/kalzium/src/periodictableview.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/isotope.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/kalziumutils.h,
	  branches/KDE/3.5/kdeedu/kalzium/src/detailinfodlg.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/spectrumwidget.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/element.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/kalziumtip.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/kalzium.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/detailedgraphicaloverview.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/kalziumutils.cpp: Show
	  numeric values in local format. Patch reviewed by Pino Toscano.
	  BUG: 122760

2007-04-02 19:12 +0000 [r649511]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/ChangeLog,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.h: Message box
	  that will tell the user that he/she has finished the last
	  training level. (Backported from kde 4)

2007-04-03 20:59 +0000 [r650108]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/training/german.ktouch.xml: fix
	  training file (bug 143763)

2007-04-05 22:29 +0000 [r650959]  mueller

	* branches/KDE/3.5/kdeedu/kstars/kstars/timespinbox.h,
	  branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatepicker.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/csegment.h,
	  branches/KDE/3.5/kdeedu/kig/modes/popup.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/kstars.h,
	  branches/KDE/3.5/kdeedu/kig/objects/bogus_imp.h,
	  branches/KDE/3.5/kdeedu/kig/misc/kigpainter.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/ksplanet.h,
	  branches/KDE/3.5/kdeedu/kiten/ksaver.cpp,
	  branches/KDE/3.5/kdeedu/kig/modes/linkslabel.h,
	  branches/KDE/3.5/kdeedu/kig/misc/lists.h,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kverbedit.h,
	  branches/KDE/3.5/kdeedu/kalzium/src/elementdataviewer.h,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/parser.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlectureeditor.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/geolocation.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatisticsdata.h,
	  branches/KDE/3.5/kdeedu/kiten/kloader.cpp,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kverbosdoc.h,
	  branches/KDE/3.5/kdeedu/kig/filters/exporter.cc,
	  branches/KDE/3.5/kdeedu/kig/objects/other_imp.h,
	  branches/KDE/3.5/kdeedu/kig/objects/common.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/skypoint.h,
	  branches/KDE/3.5/kdeedu/kig/kig/kig.h,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kverbosuser.h,
	  branches/KDE/3.5/kdeedu/kig/objects/point_imp.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchlecture.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchcolorscheme.h,
	  branches/KDE/3.5/kdeedu/kalzium/src/spectrum.h,
	  branches/KDE/3.5/kdeedu/kig/misc/object_hierarchy.cc,
	  branches/KDE/3.5/kdeedu/kalzium/src/spectrumwidget.h,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/verbspanish.h,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquizdoc.h,
	  branches/KDE/3.5/kdeedu/libkdeedu/kdeeduui/kdeeduglossary.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeys.h,
	  branches/KDE/3.5/kdeedu/kiten/rad.h,
	  branches/KDE/3.5/kdeedu/kig/kig/kig_part.h,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/keditparametric.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/ksmoon.h,
	  branches/KDE/3.5/kdeedu/keduca/libkeduca/fileread.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/ksplanetbase.h,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kfeedercontrol.h,
	  branches/KDE/3.5/kdeedu/kig/modes/typesdialog.cpp,
	  branches/KDE/3.5/kdeedu/kig/misc/rect.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchleveldata.h,
	  branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatetime.h,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/editfunction.h,
	  branches/KDE/3.5/kdeedu/kig/misc/object_hierarchy.h,
	  branches/KDE/3.5/kdeedu/kig/kig/kig_commands.cpp,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/wqlreader.h,
	  branches/KDE/3.5/kdeedu/keduca/keduca/keducaview.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.h,
	  branches/KDE/3.5/kdeedu/kig/misc/cubic-common.h,
	  branches/KDE/3.5/kdeedu/kig/filters/latexexporter.cc,
	  branches/KDE/3.5/kdeedu/kig/misc/conic-common.h,
	  branches/KDE/3.5/kdeedu/kig/kig/kig_view.h,
	  branches/KDE/3.5/kdeedu/kig/misc/coordinate.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/planetcatalog.h,
	  branches/KDE/3.5/kdeedu/kig/misc/common.h,
	  branches/KDE/3.5/kdeedu/kverbos/kverbos/kverbos.h,
	  branches/KDE/3.5/kdeedu/kig/misc/kigpainter.h,
	  branches/KDE/3.5/kdeedu/kwordquiz/src/wqundo.h,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchkeyconnector.h,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/keditpolar.h: pedantic--

2007-04-14 05:55 +0000 [r653759]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.cpp: Changed tr
	  to i18n

2007-04-15 14:01 +0000 [r654215]  annma

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangman.h,
	  branches/KDE/3.5/kdeedu/khangman/khangman/khangman.cpp: fix bug:
	  the level was not correctly displayed in the statusbar

2007-04-21 20:09 +0000 [r656599]  scripty

	* branches/KDE/3.5/kdeedu/kverbos/kverbos/startuplogo.svgz: SVG
	  files should not be executable (goutte)

2007-04-21 20:34 +0000 [r656610]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/en.dvorak.keyboard:
	  Updated dvorak keyboard so it highlights the correct shift keys.

2007-04-21 22:47 +0000 [r656644]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/dk.keyboard: update

2007-04-22 07:25 +0000 [r656722]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/be.keyboard: update

2007-04-22 07:39 +0000 [r656723]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/hungarian.ktouch.xml
	  (removed),
	  branches/KDE/3.5/kdeedu/ktouch/training/hungarian.ktouch.xml:
	  removed hungarian training file from the (keyboard folder) and
	  fixed up some problems in the correct hungarian trainingfile.

2007-04-22 14:51 +0000 [r656858]  haavard

	* branches/KDE/3.5/kdeedu/ktouch/training/norwegian.ktouch.xml:
	  Updated norwegian training file. Included sugestions from bug
	  144342

2007-04-23 18:34 +0000 [r657286-657285]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/src/main.cpp: increase version
	  for 3.5.7 release

	* branches/KDE/3.5/kdeedu/blinken/src/main.cpp: fixed a bug,
	  increased the version :-)

2007-05-02 18:49 +0000 [r660446]  mueller

	* branches/KDE/3.5/kdeedu/kig/misc/special_constructors.h,
	  branches/KDE/3.5/kdeedu/kig/modes/label.h: the usual "daily
	  unbreak compilation"

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

