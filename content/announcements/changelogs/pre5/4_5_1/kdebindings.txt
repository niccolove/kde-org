------------------------------------------------------------------------
r1164839 | rdale | 2010-08-17 20:06:10 +0100 (dt, 17 ago 2010) | 6 lines

* Don't use rb_during_gc() to skip virtual method callbacks during virtual
  method callbacks as it's probably a bad idea and causes build problems
  with old ruby interpreters.

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r1161148 | rdale | 2010-08-09 21:36:46 +0100 (dl, 09 ago 2010) | 2 lines

* Oops the major ruby version check for build krossruby should be for 1, not 9

------------------------------------------------------------------------
r1161141 | rdale | 2010-08-09 21:08:42 +0100 (dl, 09 ago 2010) | 4 lines

* Only build krossruby if the ruby version is 1.8

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
