2007-10-10 06:07 +0000 [r723586]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: KDE 3.5.8 got
	  tagged - up ver to 1.4.8_relight-post.

2007-10-11 10:26 +0000 [r724087]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.cpp: Avoid
	  crash in scrollview debug.

2007-10-15 11:39 +0000 [r725430]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/BUGS: Remove out-of-date
	  bug -- the Paste action is now never disabled anyway.

2007-10-18 07:35 +0000 [r726596]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpselection.cpp:
	  kpSelection::setPixmap() should only emit "changed()" after all
	  fields have been recalculated -- including the transparency mask.
	  This unlikely to have any effect since usually, either
	  kpViewManager::setQueueUpdates() is active and/or
	  setFastUpdates() is _not _active (the latter uses
	  QWidget::update() which only causes a repaint when it re-renters
	  the event loop, by which time setPixmap() would have finished its
	  work). Nevertheless, this commit is important so that mysterious
	  paint errors will not occur after future code changes elsewhere.

2007-10-21 06:48 +0000 [r727599]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpselectiontransparency.h:
	  +APIDoc adapted from trunk/

2007-10-22 20:34 +0000 [r728256]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFont.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFont.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFont.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1Font.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1Font.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFont.cc:
	  Splash rework, check if font is inside clip area before rendering
	  it to a temporary bitmap. Fixes bug 150693 This change is not
	  trivial. What i did is: It is getGlyph the one that does the
	  intersection between clip area and rendering area of the font
	  instead fillGlyph2 That means some clipRes =
	  state->clip->testRect but we win more robustness against broken
	  pdf that specify HUGE fonts BUG: 150693

2007-10-29 08:47 +0000 [r730648]  mlaurent

	* branches/KDE/3.5/kdegraphics/libkscan/kscandevice.cpp: backport
	  fix mem leak

2007-10-31 23:18 +0000 [r731536]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  PDF passwords are latin1, so encode them correctly BUG: 138997

2007-11-01 08:25 +0000 [r731622]  djarvie

	* branches/KDE/3.5/kdegraphics/kview/modules/presenter/imagelistdialog.ui:
	  Make it compile

2007-11-07 17:26 +0000 [r733953]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.h: fixes for
	  CVE-2007-4352/CVE-2007-5392/CVE-2007-5493

2007-11-08 22:25 +0000 [r734456]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc: another
	  gmallocn -> gmallocn_checkoverflow Fixes crashes on erroneus pdf
	  sent by Than Ngo CCMAIL: than@redhat.com CCMAIL: mueller@kde.org

2007-11-14 17:05 +0000 [r736733]  mueller

	* branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/libdjvu/DjVuGlobal.h:
	  autoconf is not defined here, and we need stdincludes with KDE4

2007-11-23 18:16 +0000 [r740624]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  sscanf crashes on a NULL pointer so verify s is not empty. Fixes
	  bug 152710, well, not really as the scanner uses poppler, i'll
	  fix poppler in a minute BUG: 152710

2007-11-27 14:37 +0000 [r742212]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  filter out non-ascii stuff from title tag

2007-12-07 12:22 +0000 [r745962]  woebbe

	* branches/KDE/3.5/kdegraphics/kuickshow/src/filefinder.cpp: IMO
	  #if KDE_VERSION >= 220 is not needed anymore :-)

2007-12-13 22:19 +0000 [r748237]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc:
	  Yet another gmallocn to gmallocn_checkoverflow, fixes ugly exit
	  produced by page 61 of
	  http://nlp.stanford.edu/IR-book/pdf/irbookonlinereading.pdf BUGS:
	  153949

2007-12-14 21:36 +0000 [r748577]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageviewutils.cpp: In the
	  message widget, draw the icon on the right when the layout is
	  RightToLeft.

2007-12-14 22:12 +0000 [r748589]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageviewutils.cpp: Do not
	  change the offset of the show, as it does not change in themes,
	  says Diego.

2007-12-28 21:45 +0000 [r753996]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp:
	  Allow the "Presentation" action in presentation mode. BUG: 154778

2008-01-15 12:24 +0000 [r761840]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Use the local file
	  path, so remote URLs are not downloaded again. (For local files
	  there is no change.) BUG: 121785

2008-01-23 22:33 +0000 [r765387]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Gfx.cc: Draw
	  correctly underlined links BUG: 151359

2008-01-30 19:02 +0000 [r768834]  mkoller

	* branches/KDE/3.5/kdegraphics/libkscan/kscandevice.cpp: BUG:
	  119500 Avoid crash by not using 0-pointer to access QAsciiDict
	  index

2008-01-31 21:05 +0000 [r769210]  mkoller

	* branches/KDE/3.5/kdegraphics/libkscan/previewer.cpp,
	  branches/KDE/3.5/kdegraphics/libkscan/scanparams.cpp: Beautify
	  the layout of the scan dialog

2008-01-31 22:56 +0000 [r769259]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc:
	  Ensure we don't draw outside the main bitmap on
	  SplashOutputDev::setSoftMask BUG: 157000

2008-02-06 21:19 +0000 [r771757]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: increase version for
	  kde 3.5.9

2008-02-07 22:41 +0000 [r772156]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc: PDF spec
	  says name tokens have a maximum length of 127, but there are some
	  docs in the wild with more than that and they work on acroread so
	  increase the allowed length to 128+127

2008-02-09 11:23 +0000 [r772669-772668]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc:
	  makeItCompile!!!

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Function.cc: atan
	  operator must yield a degrees result between 0 and 360 BUG:
	  157497

2008-02-09 12:40 +0000 [r772693]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Function.cc: PS cos
	  and sin input values are degrees so convert to radians so we can
	  use the cos and sin cmath functions CCBUG: 157497

2008-02-09 16:59 +0000 [r772886]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxState.cc: Domain
	  order is x_min x_max y_min y_max and not x_min y_min x_max y_max

2008-02-09 23:00 +0000 [r773029]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc: Only check
	  if we are out of bounds if the object we are searching for has a
	  known id (0 also means not known)

2008-02-13 09:53 +0000 [r774470]  coolo

	* branches/KDE/3.5/kdegraphics/kdegraphics.lsm: 3.5.9

