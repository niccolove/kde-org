2005-05-24 12:10 +0000 [r417716]  adrian

	* kommander/widgets/execbutton.h: fix compile with current gcc

2005-06-27 15:18 +0000 [r429401]  binner

	* VERSION, kdewebdev.lsm: 3.4.2 preparations

2005-06-27 15:38 +0000 [r429402]  binner

	* quanta/quanta.lsm, quanta/VERSION, quanta/src/quanta.h: more
	  RELEASE-CHECKLIST fun

2005-07-02 05:55 +0000 [r430663]  binner

	* kimagemapeditor/arealistview.cpp: fix disabled icons

2005-07-02 18:05 +0000 [r430920]  amantia

	* quanta/utility/toolbartabwidget.cpp,
	  quanta/treeviews/basetreeview.cpp, quanta/src/kqapp.cpp,
	  quanta/src/quanta_init.cpp, quanta/utility/toolbartabwidget.h,
	  quanta/src/document.cpp, quanta/utility/quantabookmarks.cpp,
	  quanta/utility/quantacommon.cpp, quanta/src/quantadoc.cpp,
	  quanta/utility/quantacommon.h, quanta/src/quantaview.cpp,
	  quanta/project/project.cpp, quanta/utility/tagaction.cpp,
	  quanta/quanta.kdevelop, quanta/src/quanta.cpp,
	  quanta/src/quantaview.h, quanta/project/projectnewgeneral.cpp,
	  quanta/src/quanta.h: Backport fixes from trunk: - correctly use
	  the icon/text position on toolbars - fix entity autocompletion -
	  fix double insertion of name in the tooltip of treeview items
	  Also try to reduce the diff between trunk and branch.

2005-07-05 09:04 +0000 [r431788]  amantia

	* quanta/project/uploadprofiles.cpp, quanta/quanta.kdevelop,
	  quanta/ChangeLog: Backport: show an upload treeview for profiles
	  that have @ (at sign) in the username.

2005-07-05 09:08 +0000 [r431791]  amantia

	* quanta/src/quantaview.cpp: Backport: just some safety check,
	  maybe it will avoid #107103.

2005-07-05 09:56 +0000 [r431809]  amantia

	* quanta/components/cvsservice/cvsservice.cpp, quanta/ChangeLog:
	  Backport: use unique action names. Remove not-needed ifdef's.

2005-07-05 10:44 +0000 [r431826]  amantia

	* quanta/src/quanta.cpp, quanta/src/quanta_init.cpp,
	  quanta/ChangeLog: Backport: Open the files with the right
	  encoding when restoring the last opened files on startup.

2005-07-05 12:10 +0000 [r431855]  amantia

	* quanta/src/document.cpp, quanta/ChangeLog: Backport: add missing
	  romanian accented chars to the automatic conversion list.
	  Unfortunately they cannot be added to the list of special
	  characters for 3.4.x, as it would introduce new strings. CCBUG:
	  107549

2005-07-05 13:18 +0000 [r431879]  amantia

	* quanta/project/projectupload.cpp, quanta/ChangeLog: Backport:
	  make single file/folder upload really quick, do not scan the
	  whole project dir if not needed.

2005-07-05 13:55 +0000 [r431895]  amantia

	* quanta/src/quanta.cpp, quanta/ChangeLog: Backport: paste as HTML
	  Encoded encodes more chars to their corresponding entities
	  [#100547, #100103]

2005-07-13 15:40 +0000 [r434282]  amantia

	* quanta/scripts/meinproc.kmdr, quanta/scripts/checkxml.kmdr,
	  quanta/scripts/xmlval.kmdr, quanta/ChangeLog: Fix the Kommander
	  XML scripts.

2005-07-17 09:47 +0000 [r435525]  amantia

	* quanta/src/document.h, quanta/src/document.cpp, quanta/ChangeLog:
	  Backport: improve the automatic conversion of characters. Only
	  those are converted that cannot be encoded in the encoding of the
	  document

2005-07-17 10:19 +0000 [r435531]  amantia

	* quanta/parsers/tag.cpp, quanta/parsers/saparser.cpp,
	  quanta/ChangeLog: Backports: - treat "<" as the end of a tag
	  name. Fixes autoreplacement of opening/closing tags in cases when
	  a PHP area starts directly after a tag name. - fix off by one
	  error when parsing after the a closing structure area (like "}"
	  in PHP)

2005-07-18 14:24 +0000 [r435883]  amantia

	* quanta/scripts/meinproc.kmdr, quanta/scripts/checkxml.kmdr: Fixes
	  in the docbook kommander scripts by Carlos: "This patch changes
	  the radio button to a (more appropriate) checkbox in the meinproc
	  script, and change the selection from file to directory in the
	  checkXML script." CCMAIL: carloswoelz@imap-mail.com

