---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---

### Baloo

- [baloo_file] Wait for extractor process to start
- [balooctl] Add command to show files failed to index (bug 406116)
- Add QML to source code types
- [balooctl] Capture the constant totalsize in the lambda
- [balooctl] Switch multiline output to new helper
- [balooctl] Use new helper in json output
- [balooctl] Use new helper for simple format output
- [balooctl] Factor out file index status collection from output
- Keep empty Json metadata docs out of DocumentData DB
- [balooshow] Allow referencing files by URL from hardlink
- [balooshow] Suppress warning when URL refers to unindexed file
- [MTimeDB] Allow timestamp newer than the newest document in range match
- [MTimeDB] Use exact match when exact match is requested
- [balooctl] Cleanup handling of different positional arguments
- [balooctl] Extend options help text, improve error checking
- [balooctl] Use more understandable names for size in status output
- [balooctl] clear command: Remove bogus check for documentData, cleanup
- [kio_search] Fix warning, add UDSEntry for "." in listDir
- Use hex notation for DocumentOperation flag enum
- Calculate total DB size correctly
- Postpone term parsing until needed, do not set both term and searchstring
- Don't add default valued date filters to json
- Use compact Json format when converting query URLs
- [balooshow] Do not print a bogus warning for a non-indexed file

### Breeze Icons

- Add non-symbolic 16px versions of find-location and mark-location
- Symlink preferences-system-windows-effect-flipswitch to preferences-system-tabbox
- Add "edit-delete-remove" icon symlink and add 22px version of "paint-none" and "edit-none"
- Use consistent default Kickoff user icon
- Add an icon for Thunderbolt KCM
- Sharpen Z's in system-suspend* icons
- Improve "widget-alternatives" icon
- Add go-up/down/next/previous-skip
- Update KDE logo to be closer to original
- Add alternatives icon

### Extra CMake Modules

- Bug fix: find c++ stl using regex
- Unconditionally enable -DQT_STRICT_ITERATORS, not just in debug mode

### KArchive

- KTar: Protect against negative longlink sizes
- Fix invalid memory write on malformed tar files
- Fix memory leak when reading some tar files
- Fix uninitialized memory use when reading malformed tar files
- Fix stack-buffer-overflow read on malformed files
- Fix null-dereference on malformed tar files
- Install krcc.h header
- Fix double delete on broken files
- Disallow copy of KArchiveDirectoryPrivate and KArchivePrivate
- Fix KArchive::findOrCreate running out of stack on VERY LONG paths
- Introduce and use KArchiveDirectory::addEntryV2
- removeEntry can fail so it's good to know if it did
- KZip: fix Heap-use-after-free in broken files

### KAuth

- Force KAuth helpers to have UTF-8 support (bug 384294)

### KBookmarks

- Add support for KBookmarkOwner to communicate if it has tabs open

### KCMUtils

- Use size hints from the ApplicationItem itself
- Fix Oxygen background gradient for QML modules

### KConfig

- Add Notify capability to KConfigXT

### KCoreAddons

- Fix wrong "Unable to find service type" warnings
- New class KOSRelease - a parser for os-release files

### KDeclarative

- [KeySequenceItem] Make the clear button have the same height as shortcut button
- Plotter: Scope GL Program to lifespan of scenegraph node (bug 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: Don't show english text to the user
- [ConfigModule] Pass initial properties in push()
- Enable glGetGraphicsResetStatus support by default on Qt &gt;= 5.13 (bug 364766)

### KDED

- Install .desktop file for kded5 (bug 387556)

### KFileMetaData

- [TagLibExtractor] Fix crash on invalid Speex files (bug 403902)
- Fix exivextractor crash with malformed files (bug 405210)
- Declare properties as meta type
- Change properties attributes for consistency
- Handle variant list in formatting functions
- Fix for Windows' LARGE_INTEGER type
- Fix (compilation) errors for Windows UserMetaData implementation
- Add missing mimetype to taglib writer
- [UserMetaData] Handle changes in attribute data size correctly
- [UserMetaData] Untangle Windows, Linux/BSD/Mac and stub code

### KGlobalAccel

- Copy container in Component::cleanUp before iterating
- Don't use qAsConst over a temporary variable (bug 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb - add Zambia holidays
- holidays/plan2/holiday_lv_lv - fix Midsummer's Day
- holiday_mu_en - Holidays 2019 in Mauritius
- holiday_th_en-gb - update for 2019 (bug 402277)
- Update Japanese holidays
- Add public holidays for Lower Saxony (Germany)

### KImageFormats

- tga: don't try to read more than max_palette_size into palette
- tga: memset dst if read fails
- tga: memset the whole palette array, not only the palette_size
- Initialize the unread bits of _starttab
- xcf: Fix uninitialized memory use on broken documents
- ras: Don't overread input on malformed files
- xcf: layer is const in copy and merge, mark it as such

### KIO

- [FileWidget] Replace "Filter:" with "File type:" when saving with a limited list of mimetypes (bug 79903)
- Newly created 'Link to Application' files have a generic icon
- [Properties dialog] Use the string "Free space" instead of "Disk usage" (bug 406630)
- Fill UDSEntry::UDS_CREATION_TIME under linux when glibc &gt;= 2.28
- [KUrlNavigator] Fix URL navigation when exiting archive with krarc and Dolphin (bug 386448)
- [KDynamicJobTracker] When kuiserver isn't available, also fall back to widget dialog (bug 379887)

### Kirigami

- [aboutpage] hide Authors header if there are no authors
- Update qrc.in to match .qrc (missing ActionMenuItem)
- Make sure we don't squeeze out the ActionButton (bug 406678)
- Pages: export correct contentHeight/implicit sizes
- [ColumnView] Also check for index in child filter..
- [ColumnView] Don't let mouse back button go back beyond first page
- header has immediately the proper size

### KJobWidgets

- [KUiServerJobTracker] Track kuiserver service life time and re-register jobs if needed

### KNewStuff

- Remove pixelated border (bug 391108)

### KNotification

- [Notify by Portal] Support default action and priority hints
- [KNotification] Add HighUrgency
- [KNotifications] Update when flags, urls, or urgency change
- Allow to set urgency for notifications

### KPackage Framework

- Add missing properties in kpackage-generic.desktop
- kpackagetool: read kpackage-generic.desktop from qrc
- AppStream generation: make sure we look up for the package structure on packages that have metadata.desktop/json too

### KTextEditor

- Review kate config pages to improve maintenance friendliness
- Allow to change the Mode, after changing the Highlighting
- ViewConfig: Use new generic config interface
- Fix bookmark pixmap painting on icon bar
- Ensure the left border miss no change of the count of line number digits
- Fix to show folding preview when move the mouse from bottom to top
- Review IconBorder
- Add input methods to input method status bar button
- Paint the folding marker in proper color and make it more visible
- remove default shortcut F6 to show icon border
- Add action to toggle folding of child ranges (bug 344414)
- Retitle button "Close" to "Close file" when a file has been removed on disk (bug 406305)
- up copy-right, perhaps that should be a define, too
- avoid conflicting shortcuts for switching tabs
- KateIconBorder: Fix folding popup width and height
- avoid view jump to bottom on folding changes
- DocumentPrivate: Respect indention mode when block selection (bug 395430)
- ViewInternal: Fix makeVisible(..) (bug 306745)
- DocumentPrivate: Make bracket handling smart (bug 368580)
- ViewInternal: Review drop event
- Allow to close a document whose file was deleted on disk
- KateIconBorder: Use UTF-8 char instead of special pixmap as dyn wrap indicator
- KateIconBorder: Ensure Dyn Wrap Marker are shown
- KateIconBorder: Code cosmetic
- DocumentPrivate: Support auto bracket in block selection mode (bug 382213)

### KUnitConversion

- Fix l/100 km to MPG conversion (bug 378967)

### KWallet Framework

- Set correct kwalletd_bin_path
- Export path of kwalletd binary for kwallet_pam

### KWayland

- Add CriticalNotification window type to PlasmaShellSurface protocol
- Implement wl_eglstream_controller Server Interface

### KWidgetsAddons

- Update kcharselect-data to Unicode 12.1
- KCharSelect's internal model: ensure rowCount() is 0 for valid indexes

### KWindowSystem

- Introduce CriticalNotificationType
- Support NET_WM_STATE_FOCUSED
- Document that modToStringUser and stringUserToMod only deal with english strings

### KXMLGUI

- KKeySequenceWidget: Don't show English strings to the user

### NetworkManagerQt

- WireGuard: Do not require 'private-key' to be non-empty for 'private-key-flags'
- WireGuard: workaround wrong secret flag type
- WireGuard: private-key and preshared-keys can be requested together

### Plasma Framework

- PlatformComponentsPlugin: fix plugin iid to QQmlExtensionInterface
- IconItem: remove remaining &amp; unused smooth property bits
- [Dialog] Add CriticalNotification type
- Fix wrong group names for 22, 32 px in audio.svg
- make the mobile text toolbar appear only on press
- use the new Kirigami.WheelHandler
- Add more icon sizes for audio, configure, distribute
- [FrameSvgItem] Update filtering on smooth changes
- Air/Oxygen desktoptheme: fix progressbar height using "hint-bar-size"
- Fix stylesheet support for audio-volume-medium
- Update audio, drive, edit, go, list, media, plasmavault icons to match breeze-icons
- Align z's to pixel grid in system.svg
- use the mobiletextcursor from proper namespace
- [FrameSvgItem] Respect smooth property
- Oxygen desktoptheme: add padding to hands, against jagged outline on rotation
- SvgItem, IconItem: drop "smooth" property override, update node on change
- Support gzipping of svgz also on windows, using 7z
- Air/Oxygen desktoptheme: fix hand offsets with hint-*-rotation-center-offset
- Add invokable public API for emitting contextualActionsAboutToShow
- Breeze desktoptheme clock: support hand shadow offset hint of Plasma 5.16
- Keep desktoptheme SVG files uncompressed in repo, install svgz
- separate mobile text selection to avoid recursive imports
- Use more appropriate "Alternatives" icon and text
- FrameSvgItem: add "mask" property

### Prison

- Aztec: Fix padding if the last partial codeword is all one bits

### QQC2StyleBridge

- Avoid nesting Controls in TextField (bug 406851)
- make the mobile text toolbar appear only on press
- [TabBar] Update height when TabButtons are added dynamically
- use the new Kirigami.WheelHandler
- Support custom icon size for ToolButton
- It compile fine without foreach

### Solid

- [Fstab] Add support for non-network filesystems
- [FsTab] Add cache for device file system type
- [Fstab] Preparatory work for enabling filesystems beyond NFS/SMB
- Fix no member named 'setTime_t' in 'QDateTime' error while building (bug 405554)

### Syntax Highlighting

- Add syntax highlighting for fish shell
- AppArmor: don't highlight variable assignments and alias rules within profiles

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
