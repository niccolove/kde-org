---
aliases:
- ../../plasma-5.1
date: '2014-10-15'
description: Plasma 5.1 Brings Back Many Popular Features.
layout: plasma
title: Plasma 5.1 Brings Back Many Popular Features
---

{{<figure src="/announcements/plasma/5/5.1.0/plasma-main.png" class="text-center float-right ml-3" caption="Plasma 5" width="600px" >}}

October 15, 2014. Today, KDE releases Plasma 5.1.0, the first release containing new features since the release of Plasma 5.0 this summer. Plasma 5.1 sports a wide variety of improvements, leading to greater stability, better performance and new and improved features. Thanks to the feedback of the community, KDE developers were able to package a large number of fixes and enhancements into this release, among which more complete and higher quality artwork following the new-in-5.0 Breeze style, re-addition of popular features such as the Icon Tasks taskswitcher and improved stability and performance.

Those travelling regularly will enjoy better support for time zones in the panel's clock, while those staying at home a revamped clipboard manager, allowing you to easily get at your past clipboard's content. The Breeze widget style is now also available for Qt4-based applications, leading to greater consistency across applications. The work to support Wayland as display server for Plasma is still ongoing, with improved, but not complete support in 5.1. <a href="https://community.kde.org/Plasma/5.1_Changes">Changes</a> throughout many default components improve accessibility for visually impaired users by adding support for screenreaders and improved keyboard navigation.

Aside from the visual improvements and the work on features, the focus of this release lies also on stability and performance improvements, with over 180 bugs resolved since 5.0 in the shell alone. Plasma 5.1 requires <a href="https://www.kde.org/announcements/kde-frameworks-5.3.0.php">KDE Frameworks 5.3</a>, which brings in another great amount of fixes and performance improvements on top of the large number of fixes that have gone into Plasma 5.1. If you want to help to make more of this happen, consider <a href='https://www.kde.org/fundraisers/yearend2014'>a donation</a> to KDE, so we can support more developers getting together to make great software.

- <a href='https://community.kde.org/Plasma/5.1_Changes'>5.1 Changes List</a>

## Artwork and Visuals

{{<figure src="/announcements/plasma/5/5.1.0/qt4_widgets_plus_icontasks.jpg" class="text-center" caption="Breeze Theme for Qt 4" width="600px" >}}

A new Breeze widget theme for Qt 4 lets applications written with KDE Platform 4 fit in with your Plasma 5 desktop.

The Breeze artwork concept, which has made its first appearance in Plasma 5.0 has seen many improvements. The icon set is now more complete. The icons in the notification area in the panel have been touched up visually. A new native widget style improves rendering of applications used in Plasma. This new native style also works for Qt 4 letting applications written with KDE Platform 4 fit in with your Plasma 5 desktop. There is a <a href="/announcements/plasma/5/5.1.0/plasma-lookandfeel.png">new System Settings module</a> that lets you switch between desktop themes.

Overall, Plasma 5.1's Look and Feel refines the experience found in 5.0 noticeably. Behind all these changes are improvements to the <a href="https://techbase.kde.org/Projects/Usability/HIG">Human Interface Guidelines</a>, which have led to a more consistent overall user experience.

## New and Old Features

{{<figure src="/announcements/plasma/5/5.1.0/icons_task_manager.jpg" class="text-center" caption="Icons-only Task Manager" width="600px" >}}

Plasma 5.1 brings back many features that users have grown used to from its 4.x predecessor. Popular additional widgets such as the <em>Icons-only Task Manager</em>, the <em>Notes</em> widget and the <em>System Load Viewer</em> make their re-entry. Support for multiple time zones has been added back in the panel's clock. The notifications have been visually improved, along with many bigger and smaller bug fixes.

{{<figure src="/announcements/plasma/5/5.1.0/alt_switcher.jpg" class="text-center" caption="Applet Switcher" width="600px" >}}

A new feature allow you to easily switch between different widgets which share the same purpose. Changing the application launcher for example has become much easier to discover. Plasma panels have new switchers to easily swap between different widgets for the same task. You can select which application menu, clock or task manager you want with ease. The new <em>Clipboard</em> widget offers a redesigned user interface on top of Plasma's venerable clipboard manager, allowing the user to easily use the clipboard's history and preview files currently in the clipboard. Plasma's alternative launcher, <em>Kicker</em> has seen a large number of <a href="https://community.kde.org/Plasma/5.1_Changes#Kicker_Application_Menu">improvements</a>, among which better accessibility and integration with the package manager.

Thanks to two Google Summer of Code projects, the Plasma Media Center and tablet-centric Plasma Active user experiences now have basic ports available from Git, but are not release-quality yet.

## Wayland

Further progress has been made on Wayland support. A new window manager binary 'kwin_wayland' now complements the existing 'kwin_x11', and is equipped with the ability to start a nested X server for compatibility with X11-based applications. A newly-created KWayland library provides Wayland setup information to KInfoCenter and other consumers. More work is needed and ongoing to run the Plasma workspace on Wayland; we expect this to bear fruit for end-users in 2015.

## Suitability and Updates

Plasma 5.1 provides a core desktop with a feature set that will suffice for many users. The development team has concentrated on tools that make up the central workflows. While many features known from the Plasma 4.x series are already available in Plasma 5.1, not all of them have been ported and made available for Plasma 5 yet. As with any software release of this size, there may be bugs that make a migration to Plasma 5 hard for some users. The development team would like to hear about issues you may run into, so they can be addressed and fixed. We have compiled a <a href="https://community.kde.org/Plasma/5.1_Errata">list of problems</a> we are aware of, and working on. Users can expect monthly bugfix updates. A release bringing new features and brinding back even more old features will be made in early 2015.

- <a href='https://community.kde.org/Plasma/5.1_Errata'>5.1 Known Bugs</a>
