---
aliases:
- ../../plasma-5.7.2
changelog: 5.7.1-5.7.2
date: 2016-07-19
layout: plasma
youtube: A9MtFqkRFwQ
figure:
  src: /announcements/plasma/5/5.7.0/plasma-5.7.png
  class: text-center mt-4
asBugfix: true
---

- Breeze fixed kdelibs4 compilation. <a href="http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=47a397ebef7a636497e75a8da81afffbffa30dda">Commit.</a>
- Fix startup-to-window matchup based on AppName. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1fd011ae918d241140dcbbcddf7bde2d06f1b608">Commit.</a>
