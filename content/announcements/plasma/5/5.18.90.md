---
aliases:
- ../../plasma-5.18.90
changelog: 5.18.5-5.18.90
date: 2020-05-14
layout: plasma
noinfo: true
title: 'Plasma 5.19 Beta: Consistency, Control and Usability'
---

{{< laptop src="/announcements/plasma/5/5.19.0/plasma-5.19-2.png" alt="KDE Plasma 5.19 Beta" >}}

{{% i18n_date %}}

It's time to test the beta release for Plasma 5.19!

In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.

Read on to discover all the new features and improvements of Plasma 5.19…

## Plasma Desktop and Widgets

- We have improved the panel spacer so that it can automatically center widgets.
- The System Monitor widgets have been rewritten from scratch.

{{<figure src="/announcements/plasma/5/5.19.0/sensors.png" alt="Rewritten System Monitor Widgets" caption="Rewritten System Monitor Widgets" width="600px" >}}

- Plasma now has a consistent design and header area in system tray applets as well as notifications.

{{<figure src="/announcements/plasma/5/5.19.0/system-tray-applets.png" alt="Consistent System Tray Applets" caption="Consistent System Tray Applets" width="600px" >}}

- We have refreshed the look of the media playback applet in the System Tray and of Task Manager tooltips.
- There are completely new photographic avatars to choose from.

{{<figure src="/announcements/plasma/5/5.19.0/user-avatars.png" alt="Completely New User Avatars" caption="Completely New User Avatars" width="600px" >}}

- You can now see the name of the creator of a desktop wallpaper when you go to pick one.
- Sticky notes get usability improvements.
- You now have more control over the visibility of volume OSDs during certain situations.
- GTK 3 applications immediately apply a newly selected color scheme and GTK 2 applications no longer have broken colors.
- We have increased the default fixed-width font size from 9 to 10.
- The audio widget shows a more consistent appearance with a more attractive interface for switching the current audio device.

{{<figure src="/announcements/plasma/5/5.19.0/multi-device-switching-ui.png" alt="More consistent appearance for switching the current audio device" width="600px" >}}

## System Settings

- Default Applications, Online Accounts, Global Shortcuts, KWin Rules and Background Services settings pages have all been overhauled.

{{<figure src="/announcements/plasma/5/5.19.0/redesigned-kcms.png" alt="Redesigned Settings Pages" caption="Redesigned Settings Pages" width="600px" >}}

- When launching System Settings modules from within KRunner or the application launcher, the complete System Settings application launches on the page you asked for.

{{< video src-webm="/announcements/plasma/5/5.19.0/krunner.webm" caption="Full System Settings App Is Now Launching" >}}

- The Display settings page now shows the aspect ratio for each available screen resolution.
- You now have more granular control over Plasma's animation speed.
- We have added configurable file indexing for individual directories and you can now disable indexing for hidden files.
- There is now an option that lets you configure the mouse and touchpad scroll speed under Wayland.
- We have made lots of small improvements to the font configuration.

## Info Center

- The Info Center application has been redesigned with a look and feel that is consistent with the System Settings.
- It is now possible to see information about your graphics hardware.

{{<figure src="/announcements/plasma/5/5.19.0/kinfocenter.png" alt="Redesigned Info Center" caption="Redesigned Info Center" width="600px" >}}

## KWin Window Manager

- The new subsurface clipping for Wayland greatly reduces the flickering in many applications.
- Icons in titlebars are now recolored to fit the color scheme instead of sometimes being hard to see.
- Screen rotation for tablets and convertable laptops now works on Wayland.

{{<figure src="/announcements/plasma/5/5.19.0/recoloured-icons.png" alt="Icon Recoloring in the Titlebar" caption="Icon Recoloring in the Titlebar" width="600px" >}}

## Discover

- Flatpak repositories in use are easier to remove now.
- Discover displays the application version for reviews.
- Discover improved its visual consistency and usability.

{{<figure src="/announcements/plasma/5/5.19.0/discover.png" alt="Flatpak Repository Removal in Discover" caption="Flatpak Repository Removal in Discover" width="600px" >}}

## KSysGuard

- Our system monitor KSysGuard has gained support for systems with more than 12 CPU cores.
