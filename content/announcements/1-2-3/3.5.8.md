---
aliases:
- ../announce-3.5.8
custom_about: true
date: '2007-10-16'
description: Project Ships Eighth Translation and Service Release for Leading Free
  Software Desktop.
title: KDE 3.5.8 Release Announcement
---

<h3 align="center">
   KDE Project Ships Eighth Translation and Service Release for Leading Free
   Software Desktop
</h3>

<p align="justify">
  KDE 3.5.8 features translations in 65 languages, improvements to KDE PIM
  suite and other applications.
</p>

<p align="justify">
  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.5.8,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. KDE now
  supports <a href="http://l10n.kde.org/stats/gui/stable/">65 languages</a>,
  making it available to more people than most non-free
  software and can be easily extended to support others by communities who wish
  to contribute to the open source project.
</p>
<?php // Changed stuff from 3.5.7 from here. ?>

<p align="justify">
    While the developers' main focus lies on finishing KDE 4.0, the stable 3.5 series 
    remains the desktop of choice for the time being. It is proven, stable and well
    supported. The 3.5.8 release with its literally hundreds of bugfixes has again
    improved the users' experience. The main focus of improvements for KDE 3.5.8 is
        
  <ul>
      <li>
      Improvements in Konqueror and its web browsing component KHTML. Bugs in handling 
      HTTP connections have been fixed, KHTML has improved support
      of some CSS features for more standards compliance.
      </li>
      <li>
      In the kdegraphics package, lots of fixes in KDE's PDF viewer and Kolourpaint, a painting
      application, went into this release.
      </li>
      <li>
      The KDE PIM suite has, as usual, seen numerous stability fixes, covering KDE's email client
      KMail, the organizer application KOrganizer and various other bits and pieces.
      </li>
  </ul>
 
</p>

<p align="justify">
  For a more detailed list of improvements since the
  <a href="http://www.kde.org/announcements/announce-3.5.7">KDE 3.5.7 release</a>
  on the 22nd May 2007, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_7to3_5_8">KDE 3.5.8 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.8 ships with a basic desktop and fifteen other packages (PIM,
  administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Distributions shipping KDE
</h4>
<p align="justify">
  Most of the Linux distributions and UNIX operating systems do not immediately
  incorporate new KDE releases, but they will integrate KDE 3.5.8 packages in
  their next releases. Check 
  <a href="http://www.kde.org/download/distributions">this list</a> to see
  which distributions are shipping KDE.
</p>

<h4>
  Installing KDE 3.5.8 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.8 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/3.5.8">KDE 3.5.8 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.8
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.8 may be
  <a href="http://download.kde.org/stable/3.5.8/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.8
  are available from the <a href="/info/3.5.8">KDE
  3.5.8 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> 
project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether it's
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="/community/awards/">award-winning</a>, independent <a href="/people/">project of hundreds</a>
  of developers, translators, artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  KDE provides a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr />
