---
aliases:
- ../4.0
title: KDE 4.0 Released
date: "2008-01-11"
description: KDE Community Ships Fourth Major Version for Leading Free Software Desktop.
layout: single
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Fourth Major Version of cutting edge Free Software Desktop
</h3>
<p align="justify">
With the fourth major version, the KDE Community marks the beginning of the KDE 4 era.
</p>

<p>
The KDE Community is thrilled to announce the immediate availability of
<a href="/announcements/4.0/">KDE 4.0</a>. This significant
release marks both the end of the long and intensive development cycle
leading up to KDE 4.0 and the beginning of the KDE 4 era.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>The KDE 4.0 desktop</em>
</div>
<br/>

<p>
The KDE 4 Libraries have seen major improvements in almost all areas.
The Phonon multimedia framework provides platform independent multimedia support to all
KDE applications, the Solid hardware integration framework makes interacting with
(removable) devices easier and provides tools for better power management.
<p />
The KDE 4 Desktop has gained some major new capabilities. The Plasma desktop shell
offers a new desktop interface, including panel, menu and widgets on the desktop
as well as a dashboard function. KWin, the KDE Window manager, now supports advanced
graphical effects to ease interaction with your windows.
<p />
Lots of KDE Applications have seen improvements as well. Visual updates through
vector-based artwork, changes in the underlying libraries, user interface
enhancements, new features, even new applications -- you name it, KDE 4.0 has it.
Okular, the new document viewer and Dolphin, the new file manager are only two
applications that leverage KDE 4.0's new technologies.
<p />
<img src="/announcements/4/4.0/images/oxybann.png" align="right" hspace="10"/>
The Oxygen Artwork team provides a breath of fresh air on the desktop.
Nearly all the user-visible parts of the KDE desktop and applications have been given a
facelift. Beauty and consistency are two of the basic concepts behind Oxygen.
</p>

<h3>Desktop</h3>
<ul>
	<li>Plasma is the new desktop shell. Plasma provides a panel, a menu and other
	  intuitive means to interact with the desktop and applications.
	</li>
	<li>KWin, KDE's proven window manager, now supports advanced compositing features.
	  Hardware accelerated painting takes care of a smoother and more intuitive interaction
	  with windows.
	</li>
	<li>Oxygen is the KDE 4.0 artwork. Oxygen provides a consistent, easy on the eye and
	    beautiful artwork concept.
	</li>
</ul>
Learn more about KDE's new desktop interface in the <a href="./guide">KDE 4.0 
Visual Guide</a>.

<h3>Applications</h3>
<ul>
	<li>Konqueror is KDE's established web browser. Konqueror is light-weight, well integrated,
	    and supports the newest standards such as CSS 3.</li>
	<li>Dolphin is KDE's new file manager. Dolphin has been developed with usability in mind 
		and is an easy-to-use, yet powerful tool.
	</li>
	<li>With System Settings, a new control center interface has been introduced. The
	    KSysGuard system monitor makes it easy to monitor and control system resources
	    and activity.
	</li>
	<li>Okular, the KDE 4 document viewer, supports many formats.
	    Okular is one of the many KDE 4 applications that has been improved
	    in collaboration with the <a href="http://openusability.org">OpenUsability Project</a>.
	</li>
	<li>Educational Applications are among the first applications that have been ported and
	    developed using KDE 4 technology. Kalzium, a graphical periodic table of elements
	    and the Marble Desktop Globe are only two of many gems among the educational
	    applications. Read more about Educational Applications in the 
		<a href="./education">Visual Guide</a>.
	</li>
	<li>Lots of the KDE Games have been updated. KDE Games such as KMines, a minesweeper game
	    and KPat, a patience game have had facelifts. Thanks to new vector artwork and
	    graphical capabilities, the games have been made more resolution independent.
	</li>
</ul>
Some applications are introduced in more detail in the <a href="./applications">KDE 4.0 
Visual Guide</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Filemanager, System Settings and Menu in action</em>
</div>
<br/>

<h3>Libraries</h3>
<p>
<ul>
	<li>Phonon offers applications multimedia capabilities such as playing audio and video.
	    Internally, Phonon makes use of various backends, switchable at runtime. 
		The default backend for KDE 4.0 will be the Xine backend supplying outstanding support 
		for various formats. Phonon also allows the user to choose output devices based on the 
		type of multimedia.
	</li>
	<li>The Solid hardware integration framework integrates fixed and removable devices
	    into KDE applications. Solid also interfaces with the underlying system's
	    power management capabilities, handles network connectivity and integration of
	    Bluetooth devices. Internally, Solid combines the powers of HAL, NetworkManager and
		the BlueZ bluetooth stack, but those components are replacable without breaking 
		applications to provide maximum portability.
	</li>
	<li>KHTML is the webpage rendering engine used by Konqueror, KDE's web browser. KHTML is
	    light-weight and supports modern standards such as CSS 3. KHTML was also the first
	    engine to pass the famous Acid 2 test. 
	</li>
	<li>The ThreadWeaver library, which comes with kdelibs, provides a high-level interface
	    to make better use of today's multi-core systems, making KDE applications feel smoother
	    and more efficiently using resources available on the system.
	</li>
	<li>Being built on Trolltech's Qt 4 library, KDE 4.0 can make use of the advanced visual
	    capabilities and smaller memory footprint of this library. kdelibs provides an outstanding
		extension of the Qt library, adding large amounts of high-level functionality and convenience to
		the developer.
	</li>
</ul>
</p>
<p>KDE's <a href="http://techbase.kde.org">TechBase</a> knowledge library has more information
about the KDE libraries.</p>

<h4>Take a guided tour...</h4>
<p>
The <a href="./guide">KDE 4.0 Visual Guide</a> provides a quick overview of various new
and improved KDE 4.0 technologies. Illustrated with many screenshots, it walks you
through the different parts of KDE 4.0 and shows some of the exciting new technologies and
improvements for the user. New features of the <a href="./desktop">desktop</a> get
you started, <a href="./applications">applications</a> such as System Settings, Okular the
document viewer and Dolphin the file manager are introduced. 
<a href="./education">Educational applications</a> are shown as well as 
<a href="./games">Games</a>.
</p>

<h4>Give it a spin...</h4>
<p>
For those interested in getting packages to test and contribute, several
distributions have notified us that they will have KDE 4.0 packages available
at or soon after the release. The complete and current list can be found on the
<a href="http://www.kde.org/info/4.0">KDE 4.0 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>
<p>
The following distributions have notified us of the availability of packages or Live CDs for
KDE 4.0:

<ul>
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
       <li>
       <strong>Debian</strong> KDE 4.0 packages are available in the experimental branch.
       The KDE Development Platform will even make it into <em>Lenny</em>. Watch for
       announcements by the <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
       Rumours are that a Live CD is planned as well.
       </li>
       <li>
       <strong>Fedora</strong> will feature KDE 4.0 in Fedora 9, to be <a
       href="http://fedoraproject.org/wiki/Releases/9">released</a>
       in April, with Alpha releases being available from
       24th of January.  KDE 4.0 packages are in the pre-alpha <a
       href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> repository.
       </li>
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
       <li>
		<strong>Kubuntu</strong> packages are included in the upcoming "Hardy Heron"
		(8.04) and also made available as updates for the stable "Gutsy Gibbon" (7.10).
		A Live CD is available for trying out KDE 4.0.
		More details can be found in  the <a href="http://kubuntu.org/announcements/kde-4.0">
		announcement on Kubuntu.org</a>.
        </li>
        <li>
			<strong>Mandriva</strong> will provide packages for 
			<a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.0/Mandriva/">2008.0</a> and aims
			at producing a Live CD with the latest snapshot of 2008.1.
        </li>
        <li>
			<strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> 
			for openSUSE 10.3 
			(<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
			install</a>), openSUSE Factory 
			(<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp">one-click 
			install</a>) and openSUSE 10.2. A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
			Four Live CD</a> with these packages is also available. KDE 4.0 will be part of the upcoming 
			openSUSE 11.0 release.
        </li>
</ul>

</p>

<h2>About KDE 4</h2>
<p>
KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The Dolphin file manager,
the Okular document reader and the System Settings control center complete the basic desktop set.
<br />
KDE is built
on the KDE Libraries which provide easy access to resources on the network by means of KIO and
advanced visual capabilities through Qt4. Phonon and Solid, which are also part of the KDE Libraries
add a multimedia framework and better hardware integration to all KDE applications.
</p>
