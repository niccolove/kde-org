---
title: Plasma Workspaces 4.11 Continues to Refine User Experience
date: "2013-08-14"
hidden: true
---

August 14, 2013

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption="KDE Plasma Workspaces 4.11" width="600px" >}}

In the 4.11 release of Plasma Workspaces, the taskbar – one of the most used Plasma widgets – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>has been ported to QtQuick</a>. The new taskbar, while retaining the look and functionality of its old counterpart, shows more consistent and fluent behavior. The port also resolved a number of long standing bugs. The battery widget (which previously could adjust the brightness of the screen) now also supports keyboard brightness, and can deal with multiple batteries in peripheral devices, such as your wireless mouse and keyboard. It shows the battery charge for each device and warns when one is running low. The Kickoff menu now shows recently installed applications for a few days. Last but not least, notification popups now sport a configure button where one can easily change the settings for that particular type of notification.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption="Improved notification handling" width="600px" >}}

KMix, KDE's sound mixer, received significant performance and stability work as well as <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>full media player control support</a> based on the MPRIS2 standard.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption="The redesigned battery applet in action" width="600px" >}}

## KWin Window Manager and Compositor

Our window manager, KWin, has once again received significant updates, moving away from legacy technology and incorporating the 'XCB' communication protocol. This results in smoother, faster window management. Support for OpenGL 3.1 and OpenGL ES 3.0 has been introduced as well. This release also incorporates first experimental support for the X11 successor Wayland. This allows to use KWin with X11 on top of a Wayland stack. For more information on how to use this experimental mode see <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>this post</a>. The KWin scripting interface has seen massive improvements, now sporting configuration UI support, new animations and graphical effects and many smaller improvements. This release brings better multi-screen awareness (including an edge glow option for 'hot corners'), improved quick tiling (with configurable tiling areas) and the usual slew of bug fixes and optimizations. See <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>here</a> and <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>here</a> for more details.

## Monitor Handling and Web Shortcuts

The monitor configuration in System Settings has been <a href='http://www.afiestas.org/kscreen-1-0-released/'>replaced with the new KScreen tool</a>. KScreen brings more intelligent multi-monitor support to Plasma Workspaces, automatically configuring new screens and remembering settings for monitors manually configured. It sports an intuitive, visually-oriented interface and handles re-arranging monitors through simple drag and drop.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption="The new KScreen monitor handling" width="600px" >}}

Web Shortcuts, the easiest way to quickly find what you're looking for on the web, have been cleaned up and improved. Many were updated to use securely encrypted (TLS/SSL) connections, new web shortcuts were added and a few obsolete shortcuts removed. The process of adding your own web shortcuts has been improved as well. Find more details <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>here</a>.

This release marks the end of Plasma Workspaces 1, part of the KDE SC 4 feature series. To ease the transition to the next generation this release will be supported for at least two years. Focus of feature development will shift to Plasma Workspaces 2 now, performance improvements and bugfixing will concentrate on the 4.11 series.

#### Installing Plasma

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

{{% i18n_var"KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/%[1]s'>http://download.kde.org</a> and can also be obtained on <a href='http://www.kde.org/download/cdrom.php'>CD-ROM</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today." "4.11.0" %}}

##### Packages

{{% i18n_var"Some Linux/UNIX OS vendors have kindly provided binary packages of %[1]s for some versions of their distribution, and in other cases community volunteers have done so." "4.11.0" %}} <br />

##### Package Locations

{{% i18n_var"For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_%[1]s'>Community Wiki</a>." "4.11.0" %}}

{{% i18n_var"The complete source code for %[1]s may be <a href='/info/%[1]s.php'>freely downloaded</a>. Instructions on compiling and installing KDE software %[1]s are available from the <a href='/info/%[1]s.php#binary'>%[1]s Info Page</a>." "4.11.0" %}}

#### System Requirements

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Also Announced Today:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

This release of KDE Platform 4.11 continues to focus on stability. New features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework.




