---
title: KDE Platform 4.8 Enhances Interoperability, Introduces Touch-Friendly Components
date: "2012-01-25"
hidden: true
---

<p>
KDE proudly announces the release of <strong>KDE Platform 4.8</strong>. KDE Platform is the foundation for Plasma Workspaces and KDE Applications. It has seen significant improvements. Besides the introduction of new technology, a lot of work has been done on improving the performance and stability of the underlying structure.
</p><p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/ksecretservice-sync.png">
	<img src="/announcements/4/4.8.0/thumbs/ksecretservice-sync.png" class="img-fluid" alt="KSecretService improves interoperatbility between applications">
	</a> <br/>
	<em>KSecretService improves interoperatbility between applications</em>
</div>
<br/>

Qt Quick is making its way into Plasma Workspaces. The new Plasma Components provide a standardized API implementation of widgets with native Plasma look and feel. For example, the Device Notifier widget has been ported using these components and is now written in pure QML. KWin's Window Switcher is now also QML-based, paving the way for easier design of new window switchers. The introduction of QML into the Plasma Workspaces significantly lowers the barriers for new developers to improve the Workspaces or use Plasma as the basis for other products.

</p><p>

The new <strong>Plasma QtQuick Components</strong> bring a standard set of user interface elements to Plasma. These new components bring a standardized API and implementation of common UI elements. Plasma Components offer an efficient, elegant and powerful set of widgets, which integrate well with the underlying platform. Combined with Plasma's visual and data integration components, application development has never been easier, more fun and powerful. Plasma Components automatically adapt to the device they are used on, making it easy to share code between Desktop and mobile and touch-based applications and allowing deployment and usability on a wider range of target devices.

</p><p>

<strong>KSecretService</strong> is a new framework for sharing saved passwords and other credentials between a wider range of applications. KSecretService uses KDE's well-known interfaces through a Freedesktop-compliant API with other password systems, transparently for the user. KSecretService improves integration of KDE applications into other workspaces and allows 3rd party applications to plug into KDE's secure password saving system.

</p><p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/chat.png">
	<img src="/announcements/4/4.8.0/thumbs/chat.png" class="img-fluid" alt="KDE Telepathy's chat window">
	</a> <br/>
	<em>KDE Telepathy's chat window</em>
</div>
<br/>

<a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">KDE <strong>Telepathy</strong></a> —the new real-time communication and collaboration framework for KDE Workspaces—enters KDE Extragear with its first beta version. Since the last release, the team fixed over 100 bugs and added several new features along with visual polish. You can read more <a href="http://martys.typepad.com/blog/2012/01/kde-telepathy-03-ninja-released-first-beta.html">here</a>.

<div class="text-center">
	<a href="/announcements/4/4.8.0/telepathy.png">
	<img src="/announcements/4/4.8.0/thumbs/telepathy.png" class="img-fluid" alt="KDE Telepathy's contact list">
	</a> <br/>
	<em>KDE Telepathy's contact list</em>
</div>
<br/>

</p><p>

The KDE team is currently working on the next iteration of the KDE Development Platform under the codename "Frameworks 5". While Frameworks 5 will be mostly source compatible, it will be based on Qt5. It will provide more granularity, making it easier to use just parts of the platform, and reducing dependencies considerably.

</p>
<h4>Installing the KDE Development Platform</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.8.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.8.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.8.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.8.0">4.8 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.8.0 may be <a href="/info/4.8.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.8.0
  are available from the <a href="/info/4.8.0#binary">4.8.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>




<h2>Also Announced Today:</h2>

<h3>
<a href="../plasma">
KDE Plasma Workspaces 4.8 Gain Adaptive Power Management
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.8.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.8" />
</a>

Highlights for Plasma Workspaces include <a href="http://philipp.knechtges.com/?p=10">Kwin optimizations</a>, the redesign of power management, and integration with Activities. The first QtQuick-based Plasma widgets have entered the default installation of Plasma Desktop, with more to follow in future releases. Read the complete <a href="../plasma">Plasma Workspaces Announcement</a>.
<br />

</p>

<h3>
<a href="../applications">
KDE Applications 4.8 Offer Faster, More Scalable File Management
</a>
</h3>

<p>

<a href="../applications">
<img src="/announcements/4/4.8.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.8"/>
</a>
KDE applications released today include <a href="http://userbase.kde.org/Dolphin">Dolphin</a> with its new display engine, new <a href="http://userbase.kde.org/Kate">Kate</a> features and improvements, <a href="http://userbase.kde.org/Gwenview">Gwenview</a> with functional and visual improvements. KDE Telepathy reaches first beta milestone. <a href="http://userbase.kde.org/Marble/en">Marble's</a> new features keep arriving, among which are: Elevation Profile, satellite tracking and <a href="http://userbase.kde.org/Plasma/Krunner">Krunner</a> integration. Read the complete <a href="../applications">'KDE Applications Announcement'</a>.
<br />
</p>
