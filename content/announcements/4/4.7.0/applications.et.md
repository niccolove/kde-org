---
title: Uuendatud KDE rakendused pakuvad rohkelt vaimustavaid omadusi
date: "2011-07-27"
hidden: true
---

<p>
KDE teatab rõõmuga paljude populaarsete rakenduste uute versioonide väljalaskmisest. Kõik need rakendused, alates mängudest kuni õppevajadusi rahuldavate ja meelelahutust pakkuvate rakendusteni, on nüüd veel võimsamad, ent ometi sama hõlpsasti kasutatavad. Allpool on ära toodud mõnede täna ilmunud rakenduste olulisemad uued omadused.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/dolphin-gwenview.png">
	<img src="/announcements/4/4.7.0/thumbs/dolphin-gwenview.png" class="img-fluid" alt="KDE rakendused 4.7">
	</a> <br/>
	<em>KDE rakendused 4.7</em>
</div>
<br/>

<p>

KDE grupitöölahendus <strong>Kontact</strong> on alates 4.7 väljalaskest taas ühinenud ülejäänud KDE tarkvaraga. Enamik selle komponente on porditud Akonadi peale ning nii seepärast kui ka muu arendustöö tõttu on paranenud stabiilsus, ühenduste loomine uute teenustega ning teabevahetus rakenduste vahel. Suurim muudatus on KMail 2 väljatulek. Selle liides on jäänud varasemaga sarnaseks, kuid kogu e-posti salvestamise ja hankimise süsteem on porditud Akonadi peale.

</p><p>
KDE failihalduril <strong>Dolphin</strong> on vaikimisi klaarim välimus. Menüüriba on peidetud, kuid soovi korral saab seda lihtsalt tagasi tuua. Täiustatud on failide otsimise liidest. Dolphin pakub nüüd palju korralikumat <a href="http://vishesh-yadav.com/blog/2011/07/03/mercurial-plugin-for-dolphin-work-progress-part-1-2/">lõimimist lähtekoodi haldamise süsteemidega</a>, sealhulgas saab luua ja kloonida hoidlaid, muudatusi tõmmata ja ise üles laadida, erinevusi vaadata ja veel palju muud. Nii Dolphin kui ka Konqueror on võitnud uuest pluginast, mis võimaldab otse menüü vahendusel faile ja katalooge hinnata ja annoteerida, tagades sel moel Nepomuki võimaluste ärakasutamise.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/konqueror-dolphin.png">
	<img src="/announcements/4/4.7.0/thumbs/konqueror-dolphin.png" class="img-fluid" alt="Konqueror ja Dolphin 4.7-s">
	</a> <br/>
	<em>Konqueror ja Dolphin 4.7-s</em>
</div>
<br/>

<p>
Virtuaalne gloobus <strong>Marble</strong> on saanud viimasel poolel aastal palju täiustusi. See toetab nüüd hääljuhtimist, pakub kaardi loomise nõustajat ja mitmeid uusi pluginaid. Võistlusega Voice of Marble tagati tänu KDE kogukonna usinale kaasalöömisele võimalus kasutada hääljuhtimist õige mitmes keeles. Kõigest sellest kõneleb lähemalt <a href="http://edu.kde.org/marble/current_1.2">Marble visuaalne muudatuste logi</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/marble.png">
	<img src="/announcements/4/4.7.0/thumbs/marble.png" class="img-fluid" alt="Marble shines in 4.7">
	</a> <br/>
	<em>Marble särab 4.7-s</em>
</div>
<br/>

<p>
KDE pildinäitaja <strong>Gwenview</strong> on muutnud piltide haldamise veel lihtsamaks, pakkudes võimalust võrrelda kaht või enamat pilti üksteise kõrval. Vali lihtsalt sirvimisrežiimis kaks või enam pilti ning lülitu vaate- või täisekraanirežiimi. Vaaterežiimis saab soovi korral pisipildiribalt pilte juurde lisada.

<div class="text-center">
	<a href="/announcements/4/4.7.0/gwenview.png">
	<img src="/announcements/4/4.7.0/thumbs/gwenview.png" class="img-fluid" alt="Piltide võrdlemine Gwenview 4.7 abiga">
	</a> <br/>
	<em>Piltide võrdlemine Gwenview 4.7 abiga</em>
</div>
<br/>

</p><p>
Koomiksisõprade rõõmuks toetab universaalne failinäitaja Okular nüüd terve kataloogi käsitamist koomiksina.
</p><p>
Kogu maailma tähistaevahuvilistele oluline KDE rakendus KStars võimaldab nüüd ette määrata niinimetatud star hopping'u teekondi ning toetab OpenGL ja loomupärase renderdamistaustaprogrammi dünaamilist vahetamist. Taevatrajektooridele võib nüüd omistada nimesid. Komeetide trajektoore renderdatakse OpenGL režmmis.
Matemaatikud ja reaalteaduste esindajad saavad nüüd uurida kõrgema järgu funktsioone Kalgebra abil ning teada saada keemiliste elementide oksüdatsiooniastmeid Kalziumis.
</p><p>
Tarkvara arendajatele pakub KDevelop valmistaandestiile ning Pythoni interpretaatorit Krossi vahendusel. Samuti on parandatud Pythoni automaatse sõnalõpetuse võimalusi ning toetatud on lex/yacc faililaiendid.
</p><p>
KDE täiustatud tekstiredaktor Kate pakub viimistletud pluginaid ja suuremat stabiilsust. Täpsemat teavet leiab <a href="http://kate-editor.org/2011/07/09/kate-in-kde-4-7/">Kate veebileheküljelt</a>.
</p>

<h2>
DigiKam 2.0 võimaldab ära tunda nägusid, toetab piltide eri versioone, geosilte ja palju muud
</h2>
<p>
Täna näeb lisaks ilmavalgust <strong>DigiKam 2.0</strong>. Tee versioonini 2 nõudis enam kui aasta rasket arendamist. Nüüd on aga arendusmeeskond uhkusega valmis teatama uue põlvkonna Digikami sünnist. See sisaldab muu hulgas ammu soovitud nägude tuvastamise võimalust ning toetab piltide versioonide kasutamist, samuti XMP metaandmete sidecar-faile. Oluliselt on täiustatud fotode sildistamist ja tähistamist, võimalik on pöördgeosildistamine ja veel palju muud uuenduslikku. Kõige muu kõrval on parandatud 219 viga.

<div class="text-center">
	<a href="/announcements/4/4.7.0/digikam.png">
	<img src="/announcements/4/4.7.0/thumbs/digikam.png" class="img-fluid" alt="Digikam 2.0">
	</a> <br/>
	<em>Digikam 2.0</em>
</div>
<br/>

</p><p>
Digikam käib käsikäes Kipi pluginatega, mille uus väljalase täna samuti ilmus. See sisaldab uusi eksportimistööriistu kolme veebiteenuse tarbeks: Yandex.Fotki, MediaWiki ja Rajce. GPSSynci plugin pakub nüüd pöördgeokodeerimise võimalust. Ja nagu ikka, on rohkelt parandatud vigu.
Ka paljud teised täna ilmunud KDE rakendused on saanud uusi omadusi ning neis on parandatud arvukalt vigu. Lisaks on nad saanud kasu teistest KDE tarkvarakomplekti viimastest uuendustest ja täiustustest.
</p>

<h4>KDE rakenduste paigaldamine</h4>

<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.7.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.7.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.7.0/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.7.0">4.7infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.7.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.7.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.7.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4.7.0#binary">4.7.0infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.4. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>




<h2>Täna ilmusid veel:</h2>

<h3>
<a href="../plasma">
Plasma töötsoonid: KWin aitab neid kõikjal kasutada
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="KDE Plasma töötsoonid 4.7" />
</a>

Plasma töötsoonidele on kasuks tulnud ulatuslik arendustegevus KDE komposiit-aknahalduri KWin kallal ning selliste uute Qt tehnoloogiate nagu Qt Quick kasutamise võimalus. Täpsemalt kõneleb kõigest <a href="../plasma">Plasma töölaua ja Plasma Netbook 4.7 väljalasketeade</a>.
<br />

</p>

<h3>
<a href="../platform">
KDE platvorm: täiustatud multimeedia-, kiirsuhtlus- ja semantilised võimalused
</a>
</h3>

<p>
<a href="../platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="KDE arendusplatvorm 4.7.0"/>
</a>

Suur hulk KDE ja kolmanda poole tarkvara saab tulu ulatuslikust arendustööst Phononi kallal ning olulistest täiustustest semantilise töölaua komponentide seas, mille API on tublisti täienenud ning stabiilsus suurenenud. Uus KDE Telepathy raamistik võimaldab lõimida kiirsuhtluse otse töötsoonidesse ja rakendustesse. Jõudluse ja stabiilsuse paranemine peaaegu kõigis komponentides tagab etema kasutajakogemuse ning vähendab KDE platvormi 4.7 kasutavate rakenduste koormust süsteemile. Täpsemalt kõneleb kõigest <a href="../platform">KDE platvorm 4.7 väljalasketeade</a>.
<br />

</p>
