---
aliases:
- ../announce-4.0.2
date: '2008-03-05'
title: KDE 4.0.2 lanserat
---

<h3 align="center">
   KDE-projektet lanserar andra service och översättningsversionen för den ledande fria skivbordsmiljön.
</h3>
<p align="justify">
  <strong>
    KDE lanserar den andra serviceversionen av fjärde generationens fria skrivbordsmiljö. Den här versionen 
    innehåller ett antal nya funktioner i skrivbordsskalet Plasma och ett stort antal buggfixar, prestanda-
    förbättringar och uppdateringar av översättningar.
  </strong>
</p>
<p align="justify">
KDE-projektet tillkännager idag stolt KDE 4.0.2 den andra serviceversionen för den senaste generationen
av den mest avancerade och kraftfulla skrivbordsmiljön. KDE 4.0.2 levereras med den grundläggande skrivbordsmiljön
och ett antal andra paket såsom administration, nätverk, utbildning, tillbehör, multimedia, spel, grafik,
webbutveckling med mera. KDE:s prisvinnande verktyg och applikationer finns översatta till nästan 50 olika språk.
</p>

<h4>
  <a id="changes">Förbättringar</a>
</h4>
<p align="justify">
KDE 4.0.2 är en serviceversion som tillhandahåller rättningar för olika problem som har rapporterats
med hjälp av <a href="http://bugs.kde.org/">KDE:s felhanteringssystem</a> och förbättringar av nuvarande
och nya översättningar. Till KDE 4.0.2 har dokumentations- och översättningsgrupperna gjort ett undantag
mot den vanliga policyn att inte tilllåta nya funktioner i serviceversioner. Detta har möjliggjort för
utvecklarna av Plasma att introducera några nya funktioner i denna version av KDE.
<br />
Förbättringar i denna version inkluderar bland annat:
</p>
<ul>
    <li>
    Nya funktioner i Plasma. Panelens storlek och position är numera konfigurerbar. 
    dessutom har synligheten förbättrats för ett antal inställningar för att göra det
    enklare för nya användare att upptäcka hur Plasma fungerar.
    </li>
    <li>
    Översättningen till bland annat farsi och isländska har förbättrats. KDE finns
    nu tillgängligt på 50 språk.
    </li>
    <li>
    KHTML har fått ett antal fixar för sin uppritarkod samt stöd för fler sidor genom
    förbättringar i stödet för sidor som inte är helt HTML4 kompatibla.
    </li>
</ul>

<p align="justify">
Ett antal stabilitetsproblem har blivit fixade i Kopete, KDE:s direktmeddelandeklient samt
i Okular, dokumentvisaren.

<p align="justify">
 För en mer detaljerad list över förbättingar sedan KDE 4.0.1 från förra månaden se
 <a href="/announcements/changelogs/changelog4_0_1to4_0_2">KDE
4.0.2 Changelog</a>.
</p>

<p align="justify">
Ytterligare information om förbättringar i KDE 4.0.x serien se 
<a href="../4.0/">KDE 4.0
announcement</a> och <a href="/announcements/4/4.0.1">KDE 4.0.1
announcement</a>.
</p>

<h4>Installera KDE 4.0.2</h4>
<p align="justify">
  <em>Paket</em>.
  Några Linux/UNIX distributörer tillhandahåller binära paket av KDE 4.0.2 och i 
vissa fall har andra personer i omgivningaen skapat paket.
  En del av dessa binära paket finns tillgängliga från KDE:s <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.2/">http://download.kde.org</a>.
    Ytterligare binära paket såväl som uppdatering till de redan tillgänliga kan
komma att dyka upp under de närmaste veckorna.
</p>

<p align="justify">
  <a id="package_locations"><em>Var hittar jag paketen</em></a>.
  För en uppdaterad lista över de binärpaketen KDE-projektet känner till se 
<a href="/info/4.0.2">KDE 4.0.2 Info Page</a>.
</p>

<h4>
  Kompilera KDE 4.0.2 själv
</h4>
<p align="justify">
  <a id="source_code"></a><em>Källkod</em>.
  All källkod till KDE 4.0.2 kan laddas ner  <a
href="http://download.kde.org/stable/4.0.2/src/">härifrån</a>.
Instruktioner hur man kompilerar och installerar KDE 4.0.2 finns på
<a href="/info/4.0.2#binary">KDE 4.0.2 Info Page</a>.
</p>

<h4>
  Stöd KDE
</h4>
<p align="justify">
 KDE är ett <a href="http://www.gnu.org/philosophy/free-sw.html">Fri Mjukvaru</a>
projekt som existerar och växer endast tack vare hjälp från volontärer som donerar
sin tid och sitt arbete. KDE behöver alltid mer hjälp från frivilliga vare sig det
gäller kodning, buggrättning eller rapportering, hjälp med dokumentation, översättning,
marknadsföring, ekonomisk hjälp etc. All hjälp uppskattas och accepteras gladeligen. 
Se <ahref="/community/donations/">Stöd KDE</a> för mer information. </p>

<p align="justify">
Vi ser fram emot att få höra av dig snart!
</p>

<h2>Om KDE 4</h2>
<p>
KDE 4.0 är den innovativa fria skrivbordsmiljön med massor av appplikationer för alla uppgifter
i dagligt användande och för specifika uppgifter. Plasma är det nya skrivbordsskalet för KDE 4 
som tillhandahåller ett intiutivt gränssnitt för att interagera med skrivbordet och applikationerna.
Webbläsaren Konqueror integrerar Internet med skrivbordet. Filhanteraren Dolphin, dokumentvisaren 
Okular och kontrollpanelen System Settings kompletterar den grundläggande skrivbordsmiljön.
<br />
KDE är byggt på KDEs biblioteken som ger enkel åtkomst till nätverket genom KIO och avancerade
grafiska funktioner genom Qt4. Phonon och Solid, som också ingår i KDEs bibliotek tillhandahåller
stöd för multimedia och bättre hårdvaruintegration för alla KDEs applikationer.
</p>



